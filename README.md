# Whereabouts Clock

## Intro

I've been a fan of the Harry Potter series since the movies first hit the big screen in 2001. Since then I've read all the books, and seen all the movies.  I haven't made a trip to Harry Potter World at Universal Studios, but it's on my list. In a couple of the books and in the movies there are a few references to a magical clock at the Weasley's house:

The first specific description of the family clock is from Goblet of Fire (pages. 151-153):

> Mrs. Weasley glanced at the grandfather clock in the corner. Harry liked this clock. It was completely useless if you wanted to know the time, but otherwise very informative. It had nine golden hands, and each of them was engraved with one of the Weasley family’s names. There were no numerals around the face, but descriptions of where each family member might be. “Home,” “school,” and “work” were there, but there was also “traveling,” “lost,” “hospital,” “prison,” and, in the position where the number twelve would be on a normal clock, “mortal peril.”
>
> Eight of the hands were currently pointing to the “home” position, but Mr. Weasley’s, which was the longest, was still pointing to “work.”
> . . .
>
>“Oh your father’s coming!” she said suddenly, looking up at the clock again.
>
>Mr. Weasley’s hand had suddenly spun from “work” to “traveling”; a second later it had shuddered to a halt on “home” with the others, and they heard him calling from the kitchen.

I wanted a nice looking clock that would automatically track my family without having to update locations manually. My family members all have iPhones, so this should be a good source of location data.

## MQTT-Only VERSION (Aug. 2024)

After the last update I found that OwnTracks has the capability to report when a user arrives and leaves a known location.  This is how iOS can send you an alert about a Reminder when you get home.  If I could take the database of known locations out of MariaDB and load them into OwnTracks then the phones could notify the Arduino directly of the change in location.  Arduino has support for MQTT Pub/Sub as well.

This new version eliminates the MqttWarn, Apache, and MariaDB Docker containers and leaves only the Mosquitto to run locally.  This could also be done with a Raspberry Pi or any other mechanism for running Mosquitto.

So the operation now goes something like this:

1. OwnTracks on the phone will push location data per its configuration to the MQTT server (Docker container).  This information contains data on known locations to your family: home, work, etc.
2. The Arduino is a subscriber to the OwnTracks topic and immediately receives the message, parses it, and moves the clock hands accordingly.

![Whereabouts process](Whereabouts_mqttonly.jpg "Whereabouts process")

Full install and configuration steps to come....


## MQTT+DB VERSION (Jan. 2024)

Somewhere along the line Apple decided to lockdown the Find My iPhone API that I was using and forced login with MFA.  This wouldn't work with an automated tool like this, so I had to search elsewhere.  I found that Life360 had an app and folks like me had reverse engineered their API, so with a little rework I had my clock back in action.  However, it didn't last.  Life360 soon locked down their API, so I was sent in search of another alternative.  I found on a forum for Home Assistant that the app OwnTracks might be a suitable alternative.  It's a neat little app that will trigger the phone to send its location data via MQTT to a server that you specify.  I had to rework the code a bit, but now the operation is as follows:

1. OwnTracks on the phone will push location data per its configuration to the MQTT server (Docker container)
2. The Mosquitto (MQTT Broker) container will receive the data and forward the data to its subscribers
3. The MQTTWarn container is a subscriber and translates the received data into the MariaDB database
4. Each minute, the Arduino checks the Apache webserver (executing a Python script) to get the latest location data from the database.

![Whereabouts process](Whereabouts.jpg "Whereabouts process")


## OLD VERSION:

The software consists of a MySQL database, Apache Web Server, and an Arduino Sketch.  The original LAMP-stack code was written for PHP, but I've since updated it to Python and Docker containers.  I use an Arduino Mega with a Wifi shield and custom shield for the indicator LEDs and demo mode switch.

Here's the basic operation:
1. A cron job on the web server runs the fetch_locations.py script that grabs location data from our iPhones and puts it into the MySQL database
2. The Arduino polls the web server each minute using the fetch_current.py script which returns only the last location of the user.  The Arduino then updates the motors to move to the correct location.
