#!/usr/bin/env python
from life360 import life360
import logging
import time
from time import gmtime, strftime, mktime
import datetime


import yaml
import base64
import pymysql
import re
import collections

logging.basicConfig(level=logging.DEBUG,
    format='(%(threadName)-10s) %(message)s',
)

# Description:
# This script is used to query iPhone location data and populate the
# Whereabouts clock database.
#
# Global Definitions
# Don't change the rest, please!!
#
# Minimum accuracy value.  If the phone is really accurate it can throw off the
# reading since the location is just a point value.
min_accuracy = 50

# Timeout value (in sec) to trigger mortal peril
# If the last phone data is older than this, then the user is in mortal peril.
mortalperil_timeout = 7200
#
#	Calculation factors for converting accuracy of GPS coordinates from meters to degrees of
#	lat/long.  The latitude value is consistent, but the longitude value changes depending on the
#	degree of latitude you are at.	Austin, TX is pretty close to 30.4 deg. latitude, and this is
#	what the calculation is based on.  These numbers were calculated from
#	http://www.csgnetwork.com/degreelenllavcalc.html
#
deg_lat_to_meter = 110859.193020
deg_long_to_meter = 96096.951354

db = None
cursor = None

# Timestamp and datatime data for right now
current_timestamp = int(time.time())
current_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp))
expires_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp - 2592000))  # 30 day expiry
print "CURRENT TIMESTAMPS:"
print "Timestamp: " + str(current_timestamp)
print "Date Time: " + current_datetime
print "Expires Date Time: " + expires_datetime


# Load credentials file
try:
    with open("/usr/local/apache2/htdocs/.credentials.yaml", 'r') as ymlfile:
        creds = yaml.load(ymlfile, Loader=yaml.SafeLoader)
    # print yaml.dump(cfg)
except IOError as e:
   logging.critical("Could not read credential file due to I/O error({0}): {1}".format(e.errno, e.strerror))
   quit(10)
except:
   logging.critical("Unexpected error reading config file:", sys.exc_info()[0])
   quit(10)


################################################################################
#
#   FUNCTIONS
#
################################################################################


def mysql_connect():
    global db
    db = pymysql.connect(host = creds['db_hostname'],
                         user = creds['db_username'],
                         passwd = creds['db_password'],
                         db = creds['db_database'])
    cur = db.cursor(pymysql.cursors.DictCursor)
    return cur
# END mysql_connect


def mysql_disconnect():
    global db
    db.close()
# END mysql_disconnect


# Function to get the data from the user table
def _get_users():
    global cursor
    query = "SELECT * FROM user"

    result = cursor.execute(query)
    return cursor.fetchall()
# END _get_users


# Function to get the latest location entry from the DB for a specific user
def _get_last_db_entry(uid):
    global cursor

    query = "SELECT h.timestamp, unix_timestamp(h.timestamp) AS unixtime, h.latitude, h.longitude, h.accuracy, cl.name AS location \
FROM `history` `h` \
INNER JOIN `clock_location` `cl` \
	ON `h`.`clock_location_id` = `cl`.`id` \
WHERE `h`.`user_id` = %s \
ORDER BY timestamp DESC \
LIMIT 1"

    result = cursor.execute(query, (uid))
    return cursor.fetchone()
# END _get_last_db_entry


# Function to get the location data from the DB based on user, lat, long, and accuracy
def _get_db_known_locations(uid, phone_data):
    global deg_lat_to_meter
    global deg_long_to_meter
    global min_accuracy
    global cursor

    # if the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = int(phone_data['location']['accuracy'])
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = float(phone_data['location']['latitude'])  - lat_acc
    lat_max  = float(phone_data['location']['latitude'])  + lat_acc
    long_min = float(phone_data['location']['longitude']) - long_acc
    long_max = float(phone_data['location']['longitude']) + long_acc

    # Search for locations in the DB that fall within the bounding box
    query = "SELECT `cl`.`name` AS `location`, `l`.`name` \
FROM `location` `l` \
INNER JOIN `clock_location` `cl` \
    ON `cl`.`id` = `l`.`clock_location_id` \
WHERE (`l`.`latitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`longitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`user_id` = \'%s\') \
LIMIT 1"

    result = cursor.execute(query, (lat_min, lat_max, long_min, long_max, uid))

    if result > 0:
        results = cursor.fetchone()
        return results['location']
    else:
        return "unknown"
# END _get_db_known_locations


# Function to get the clock_location ID based on the name
def _get_clock_location(location):
    global cursor

    query = "SELECT `id` FROM `clock_location` WHERE `name` = %s LIMIT 1"
    result = cursor.execute(query, (location))

    if result == 1:
        results = cursor.fetchone()
        return results['id']
    else:
        return None
# END _get_clock_location


# Update the DB with the latest phone data from a user
def _update_db(uid, phone_data):
    global cursor
    global db

    # Get the clock location ID based on name
    clock_id = _get_clock_location(phone_data['location']['clock_location'])

    # Insert the new data into the DB
    query = "INSERT INTO `history` (`user_id`, `timestamp`, `latitude`, `longitude`, `accuracy`, `clock_location_id`) \
VALUES (%s, %s, %s, %s, %s, %s)"

    cursor.execute(query, (uid, phone_data['datetime'], phone_data['location']['latitude'], phone_data['location']['longitude'], phone_data['location']['accuracy'], clock_id))
    db.commit()
# END _update_db


# Function to check to see if the new phone data is near the most recent location
def _spot_check(last_data, phone_data):
    global deg_lat_to_meter
    global deg_long_to_meter
    global min_accuracy

    # If the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = int(phone_data['location']['accuracy'])
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = float(phone_data['location']['latitude'])  - lat_acc
    lat_max  = float(phone_data['location']['latitude'])  + lat_acc
    long_min = float(phone_data['location']['longitude']) - long_acc
    long_max = float(phone_data['location']['longitude']) + long_acc

    # The latest phone data was near the last data (i.e. the user is in the same place)
    if  last_data['latitude']  >= lat_min  and \
        last_data['latitude']  <= lat_max  and \
        last_data['longitude'] >= long_min and \
        last_data['longitude'] <= long_max:
            return True
    # The user has moved
    else:
        return False
# END _spot_check


# Remove old data from the history table so it doesn't get massive
def _expire_old_history():
    global db
    global cursor
    global expires_datetime

    query = "DELETE FROM `history` WHERE `timestamp` < %s"

    cursor.execute(query, (expires_datetime))
    db.commit()
    
# END _expire_old_history


def _get_life360_circle():
    #instantiate the API
    api = life360(authorization_token=creds['authorization_token'], username=creds['l360_username'], password=creds['l360_password'])
    if api.authenticate():

        #Grab some circles returns json
        circles =  api.get_circles()
        
        #grab id
        id = circles[0]['id']

        #Let's get your circle!
        return api.get_circle(id)
    else:
        raise Exception("Error authenticating to service")

# END _expire_old_history



################################################################################
#
#   MAIN
#
################################################################################

# connect to DB and return cursor
cursor = mysql_connect()

# Get the user data from the database
users = _get_users()


# go get data from Life360
try:
    circle = _get_life360_circle();
    logging.debug(circle)

except Exception as error:
    print("Error with Life360: ", error)
    quit(20)


# Loop through each user
for user in users:
    phone_data = None
    logging.info("User data from DB table: " + str(user))

    # Populate the previous data to compare against.
    last_data             = _get_last_db_entry(user['id'])
    logging.info("Last DB location data from DB: " + str(last_data))

    # Loop through each member in the family circle
    for m in circle['members']:
        if (user['username'] == m['firstName']):
            phone_data = m

    if (phone_data == None):
        logging.error("Phone data not found for user: %s", user['username'])
        continue
    
    # Get the DB time format from the phone timestamp
    phone_data['datetime'] = strftime("%Y-%m-%d %H:%M:%S", gmtime(float(phone_data['location']['timestamp'])))

    # Print a comparision of last and current data
    print "\n"
    print "{0:11} {1:26} | {2:10} {3:26}".format("LAST DATA:", user['username'], "PHONE DATA:", user['username'])
    print "--------------------------------------------------------------------------------"
    print "{0:11} {1:26} | {2:10} {3:26}".format("Timestamp:", last_data['unixtime'],  "Timestamp:", phone_data['location']['timestamp'])
    print "{0:11} {1:>26} | {2:10} {3:>26}".format("DateTime:",  str(last_data['timestamp']), "DateTime:",  str(phone_data['datetime']))
    print "{0:11} {1:26} | {2:10} {3:26}".format("Latitude:",  last_data['latitude'],  "Latitude:",  phone_data['location']['latitude'])
    print "{0:11} {1:26} | {2:10} {3:26}".format("Longitude:", last_data['longitude'], "Longitude:", phone_data['location']['longitude'])
    print "{0:11} {1:26} | {2:10} {3:26}".format("Accuracy:",  last_data['accuracy'],  "Accuracy:",  phone_data['location']['accuracy'])
    print "{0:11} {1:26} | ".format("Location:",  last_data['location']),

    # If the date in the phone is newer than our last known location, then we have a new
    # entry and will do something with it
    if (int(phone_data['location']['timestamp']) > int(last_data['unixtime'])):
        # we have a new data -- check to see where we're at.
        phone_data['location']['clock_location'] = _get_db_known_locations(user['id'], phone_data)

        print "{0:11} {1:26}".format("Location:", phone_data['location']['clock_location'])
        print "--------------------------------------------------------------------------------"
        print "{0:11} {1:26}".format("", "NEW PHONE DATA!")

        # Check to see if we're in the same location, but just have an updated entry
        if phone_data['location']['clock_location'] == last_data['location']:
            # same place, just update the log with the new timestamp
            _update_db(user['id'], phone_data)
            print "{0:>38}\n".format("No change.  Still at: " + phone_data['location']['clock_location'])

        # We've moved!  do some checking to see where we're at
        else :
            # If the new location is unknown we'll say we're traveling.
            if phone_data['location']['clock_location'] == "unknown":
                phone_data['location']['clock_location'] = "traveling"

            # check to see if phone is in an unknown location, but in same spot.
            # If we're in an unknown location (traveling), but still in the same spot we're "lost"
            if _spot_check(last_data, phone_data) and phone_data['location']['clock_location'] == "traveling" and \
                (last_data['location'] == "traveling" or last_data['location'] == "lost"):
                    phone_data['location']['clock_location'] = "lost"

            # Update the DB with the location
            _update_db(user['id'], phone_data)
            print "{0:>38}\n".format("Moved!  New loc: " + phone_data['location']['clock_location'])
        # END if

    # No new phone data -- NOTE:  Need to check if this logic works.
    else:
        print "\n{0:11} {1:26} | ".format("", "NO NEW DATA!")
        # If the timestamp is older than our mortalperil timeout, update us to mortalperil.
        if (int(phone_data['location']['timestamp']) < current_timestamp - mortalperil_timeout):
            phone_data['location']['clock_location']  = 'mortalperil'
            phone_data['datetime'] = current_datetime
            phone_data['location']['latitude']  = 00.0000
            phone_data['location']['longitude'] = 00.0000
            phone_data['location']['accuracy']  = 0
            _update_db(user['id'], phone_data)
            print "{0:38}\n".format("Timeout reached: MORTAL PERIL!")
        else:
            print "{0:38}\n".format("No new DB entry")
    # END if

# # END user loop

# Delete old entries
_expire_old_history()

# Disconnect from the DB
mysql_disconnect()
