#!/usr/bin/env python
# from __future__ import print_function
import httplib2
import os
import sys

import base64
import pymysql
import re
import collections
import json
import yaml
import datetime
from datetime import timedelta

print "Content-type: text/plain\n"

# Load credentials file
try:
    with open(".credentials.yaml", 'r') as ymlfile:
        creds = yaml.load(ymlfile, Loader=yaml.SafeLoader)
        # print yaml.dump(creds)
except IOError as e:
    print "Could not read credential file due to I/O error({0}): {1}".format(e.errno, e.strerror)
    quit(10)
except:
    print "Unexpected error reading config file:", sys.exc_info()[0]
    quit(10)


# Some global vars
db = None
flags = None
Locations = None



################################################################################
#
#   FUNCTIONS
#
################################################################################


def mysql_connect():
    global db
    db = pymysql.connect(host = creds['db_hostname'],
                         user = creds['db_username'],
                         passwd = creds['db_password'],
                         db = creds['db_database'])
    cur = db.cursor(pymysql.cursors.DictCursor)
    return cur
# END mysql_connect


def mysql_disconnect():
    global db
    db.close()
# END mysql_disconnect


# Function to get the data from the user table
def _get_users ():
    global cursor
    query = "SELECT `id`, `username`, `location_source` FROM `user`"
    rows = cursor.execute(query)

    return cursor.fetchall()
# END _get_users


# Function to get the latest location entry from the DB for a specific user
def _get_last_db_entry (uid):
    global cursor

    query = "SELECT h.timestamp, cl.name AS location \
FROM `history` `h` \
INNER JOIN `clock_location` `cl` \
	ON `h`.`clock_location_id` = `cl`.`id` \
WHERE `h`.`user_id` = %s \
ORDER BY h.timestamp DESC \
LIMIT 1"

    rows = cursor.execute(query, (uid))
    result = cursor.fetchone()

    # return the clock location only
    return result['location']
# END _get_last_db_entry


# Function to get the latest location entry from the DB for a specific user
def _get_clock_location (loc_id):
    global cursor

    query = "SELECT name AS location \
FROM `clock_location` \
WHERE `id` = %s"

    rows = cursor.execute(query, (loc_id))
    result = cursor.fetchone()

    return result['location']
# END _get_clock_location


# Function to get the latest location entry from the DB for a specific user
def _get_clock_locations ():
    global cursor
    locationlist = []

    query = "SELECT name AS location FROM `clock_location`"

    rows = cursor.execute(query)
    results = cursor.fetchall()
    for result in results:
        locationlist.append(result['location'])

    return locationlist
# END _get_clock_locations


# Get the users and their current locations from the DB
def _get_user_locations():
    data = collections.OrderedDict()

    # Get the list of users from the DB
    users = _get_users()

    # Loop through each user
    for user in users:
        if user['location_source'] == 'GPS':
            data[user['username']] = _get_last_db_entry(user['id'])
        # Add in a bunch here to do Google calendar
        else:
            # fallback to another user's location or a hard-coded location
            m = re.search('^(clock|user)_(\d+)$', user['location_source'])
            if m.group(1) == 'user':
                data[user['username']] = _get_last_db_entry(m.group(2))
            elif m.group(1) == 'clock':
                data[user['username']] = _get_clock_location(m.group(2))
            else:
                print "ERROR in specified clock location or user ID"
        # END if
    # END for

    return data
# END _get_user_locations




################################################################################
#
#   MAIN
#
################################################################################

# connect to DB and return cursor
cursor = mysql_connect()

Locations = _get_clock_locations()

# Get the latest user location
data = _get_user_locations()

# Disconnect from the DB
mysql_disconnect()

print json.dumps(data);

