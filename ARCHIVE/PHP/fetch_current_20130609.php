<?PHP
// Description:
// TBD !
//
//
// Global Definitions
// Change these for different users

// User array defined as "Name' => Hand ID on the clock (0-n)
$users = array("dad" => 0, "mom" => 1, "child1" => 2, "child2" => 3, "child3" => 4);

// Database connection information
$db_hostname = "192.168.0.50";
$db_username = "whereabouts";
$db_password = "XXXXXXX";
$db_database = "whereabouts";

// Don't change the rest, please!!


// FUNCTIONS

// Function to get the latest location entry from the DB for a specific user
// This one is different than the one in fetch_locations.php because here it also takes the
// Override table into account.
function _get_last_db_entry ($user) {
	global $db_hostname, $db_username, $db_password, $db_database, $users;
    $db = mysql_connect($db_hostname, $db_username, $db_password);
    mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());
	
	// Get the Override info for the user first.  Then we will decide what to do
	$query = "SELECT `location` FROM `Override` WHERE `user` = '".$user."'";
	$result = mysql_query($query, $db) or die("Could not query DB for override data: ".mysql_error());
	$override_arr = mysql_fetch_array($result);
	
	// Check to see if there is a scheduled location
	// Fix this to work with Google calendar
	$schedule = _get_scheduled_location($user, time());
	
	// Priority 1:  If Override is GPS, use the GPS data
	// Get the data from the GPS table if Override table said to do so
	if ($override_arr['location'] == "GPS") {
		$query = "SELECT location FROM `$user` ORDER BY timestamp DESC LIMIT 1";
		$result = mysql_query($query, $db) or die("Could not query DB for latest location: ".mysql_error());
		$results_arr = mysql_fetch_array($result);
		return trim($results_arr['location']);
	}
	// Priority 2:  If there is a scheduled location, use that
	// Get scheduled locations
	elseif ($schedule != "NONE") {
		return $schedule;
	}
	// Priority 3:  If the override data points to another user, use that
	// Get the data from another user's database location
	elseif (is_int($users[$override_arr['location']])) {
		$query = "SELECT location FROM `".$override_arr['location']."` ORDER BY timestamp DESC LIMIT 1";
		$result = mysql_query($query, $db) or die("Could not query DB for latest location: ".mysql_error());
		$results_arr = mysql_fetch_array($result);
		return trim($results_arr['location']);
	}
	// Priority 4:  Use the location provided in the Override table
	// Use the override location
	else {
		return trim($override_arr['location']);
	}
}



// Function to get a location based on a schedule
// This is the same as the function in fetch_locations.php
function _get_scheduled_location($user, $current_timestamp) {
	global $db_hostname, $db_username, $db_password, $db_database;
    $db = mysql_connect($db_hostname, $db_username, $db_password);
    mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

    $day = date("l", $current_timestamp);
    $time = date("H:i:s", $current_timestamp);

    $query = "SELECT `location` FROM `Schedules` WHERE `user` = '".$user."' AND `day` = '".$day."' AND '".$time."' BETWEEN `starttime` AND `endtime`";
    $result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

    if (mysql_num_rows($result) > 0) {
    	$results_arr = mysql_fetch_array($result);
        return $results_arr['location'];
    }
    else {
    	return "NONE";
	}
}




// -------- MAIN ---------------
// Prints some basic XML data that the Arduino can parse
print '<?xml version="1.0" encoding="ISO-8859-1"?>
<whereabouts>
';

while (list($user, $uid) = each($users)) {
	$location = _get_last_db_entry($user);
	print "<$user>$location</$user>\n";
}

print "</whereabouts>\n";
?>
