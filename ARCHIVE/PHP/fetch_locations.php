<?PHP
require 'class.sosumi.php';
// Description:
// This script is used to query iPhone location data and populate the
// Whereabouts clock database.
//
// Global Definitions
//
// Enter your iCloud username and password
$ssm = new Sosumi('my_email@address.com', 'mypass');

// Database connection information
$db_hostname = "localhost";
$db_username = "whereabouts";
$db_password = "password";
$db_database = "whereabouts";

// Don't change the rest, please!!
// Additional globals

// Minimum accuracy value.  If the phone is really accurate it can throw off the 
// reading since the location is just a point value.
$min_accuracy = 50;

// Timeout value (in sec) to trigger mortal peril
// If the last phone data is older than this, then the user is in mortal peril.
$mortalperil_timeout = 7200;

/*	
	Calculation factors for converting accuracy of GPS coordinates from meters to degrees of 
	lat/long.  The latitude value is consistent, but the longitude value changes depending on the 
	degree of latitude you are at.	Austin, TX is pretty close to 30.4 deg. latitude, and this is
	what the calculation is based on.  These numbers were calculated from
	http://www.csgnetwork.com/degreelenllavcalc.html
*/
$deg_lat_to_meter = 110859.193020;
$deg_long_to_meter = 96096.951354;


// Timestamp and datatime data for right now
$current_timestamp = time();
$current_datetime = strftime("%F %T", $current_timestamp);
$expires_datetime = strftime("%F %T", $current_timestamp - 2592000);  // 30 day expiry
print "CURRENT TIMESTAMPS:\n";
print "Timestamp: $current_timestamp\n";
print "Date Time: $current_datetime\n\n";

// Connect to the DB
$db = mysql_connect($db_hostname, $db_username, $db_password);
mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());





// FUNCTIONS

// Function to get the data from the user table
function _get_users () {
	global $db;

	$query = "SELECT * FROM user";
	$results = array();
	$q_result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	while ($results_row = mysql_fetch_array($q_result)) {
		array_push($results, $results_row);
	}
	
	return $results;
}



// Function to get the latest location entry from the DB for a specific user
function _get_last_db_entry ($uid) {
	global $db;

	$query = 'SELECT h.timestamp, h.latitude, h.longitude, h.accuracy, cl.name AS location
FROM `history` `h` 
INNER JOIN `clock_location` `cl`
	ON `h`.`clock_location_id` = `cl`.`id`
WHERE `h`.`user_id` = \''.$uid.'\'
ORDER BY timestamp DESC
LIMIT 1';

	$result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	$results_arr = mysql_fetch_array($result);

	return $results_arr;
}



// Function to get the location data from the DB based on user, lat, long, and accuracy
function _get_db_known_locations ($uid, $phone_data) {
	global $deg_lat_to_meter, $deg_long_to_meter, $min_accuracy, $db;

	// if the accuracy reporting from the phone is too small it can generate false reports.
	// Here we'll adjust the accuracy to a good minimum
	$acc = $phone_data['accuracy'];
	if ($acc < $min_accuracy) { $acc = $min_accuracy; }

	// Convert the accuracy to degrees
	$lat_acc = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min  = $phone_data['latitude']  - $lat_acc;
	$lat_max  = $phone_data['latitude']  + $lat_acc;
	$long_min = $phone_data['longitude'] - $long_acc;
	$long_max = $phone_data['longitude'] + $long_acc;
	
	// Search for locations in the DB that fall within the bounding box
	$query = 'SELECT `cl`.`name` AS `location`, `l`.`name`
FROM `location` `l`
INNER JOIN `clock_location` `cl`
	ON `cl`.`id` = `l`.`clock_location_id`
	WHERE (`l`.`latitude` BETWEEN \''.$lat_min.'\' AND \''.$lat_max.'\' AND
	   `l`.`longitude` BETWEEN \''.$long_min.'\' AND \''.$long_max.'\' AND
	   `l`.`user_id` = \''.$uid.'\')';
	$result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

	if (mysql_num_rows($result) > 0) {
		$results_arr = mysql_fetch_array($result);
		return $results_arr['location'];		
	}
	else {
		return "unknown";
	}
}



// Function to get the clock_location ID based on the name
function _get_clock_location ($location) {
	global $db;
	
	$query = "SELECT id FROM clock_location WHERE name = '$location'";
	$result = mysql_query($query, $db) or die("Could not query DB for _get_clock_location data: ".mysql_error());

	$num_rows = mysql_num_rows($result);
	if ($num_rows == 1) {
		$results_arr = mysql_fetch_array($result);
		return $results_arr['id'];		
	}
	else {
		die("Did not get only 1 row from _get_clock_location.  Rows: $num_rows");
	}
}



// Update the DB with the latest phone data from a user
function _update_db ($uid, $phone_data) {
	global $db;
//	list($datetime, $lat, $long, $acc, $location) = explode('|', $data);

	// Insert the new data into the DB
	$query = 'INSERT INTO `history` (`user_id`, `timestamp`, `latitude`, `longitude`, `accuracy`, `clock_location_id`)
VALUES (\''.$uid.'\',
\''.$phone_data['timestamp'].'\',
\''.$phone_data['latitude'].'\',
\''.$phone_data['longitude'].'\',
\''.$phone_data['accuracy'].'\',
\''._get_clock_location($phone_data['location']).'\')';

	mysql_query($query, $db) or die("Could not update DB: ".mysql_error());
}



// Function to check to see if the new phone data is near the most recent location
function _spot_check($last_data, $phone_data) {
	global $deg_lat_to_meter, $deg_long_to_meter, $min_accuracy;

	// If the accuracy reporting from the phone is too small it can generate false reports.
	// Here we'll adjust the accuracy to a good minimum
	$acc = $phone_data['accuracy'];
	if ($acc < $min_accuracy) { $acc = $min_accuracy; }
	
	// Convert the accuracy to degrees
	$lat_acc  = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min  = $phone_data['latitude']  - $lat_acc;
	$lat_max  = $phone_data['latitude']  + $lat_acc;
	$long_min = $phone_data['longitude'] - $long_acc;
	$long_max = $phone_data['longitude'] + $long_acc;
	
	// The latest phone data was near the last data (i.e. the user is in the same place)
	if ($last_data['latitude']  >= $lat_min  && $last_data['latitude']  <= $lat_max  &&
	    $last_data['longitude'] >= $long_min && $last_data['longitude'] <= $long_max) {
		return true;
	}
	// The user has moved
	else {		
		return false;
	}
}



// Remove old data from the history table so it doesn't get massive
function _expire_old_history() {
	global $db, $expires_datetime;
	$query = "DELETE FROM `history` WHERE `timestamp` < '$expires_datetime'";
	
	mysql_query($query, $db) or die("Could not remove old history entries: ".mysql_error());
}





// -------- MAIN ---------------

// Get the user data from the database
$users = _get_users();

// Loop through each user
foreach ($users as $user) {
	/*
	 / Look at the location_source field and determine what to do
	 / For GPS, get the GPS data from the phone
	 / For clock_<ID>, do nothing.  The clock code will grab that.
	 / For user_<ID>, do nothing, The clock code will handle that as well.
	 */
	if ($user['location_source'] != 'GPS') { continue; }
	// print_r($user);
	 
	// Populate the previous data to compare against.
	$last_data             = _get_last_db_entry($user['id']);
	$last_data['unixtime'] = strtotime($last_data['timestamp']);

	// Get the phone's location from the iCloud site
	$phone_data             = $ssm->locate($user['apple_id']);
	$phone_data['unixtime'] = strtotime($phone_data['timestamp']);

	// Print a comparision of last and current data
	printf("\n");
	printf("%11s %-26s | %11s %-26s\n", "LAST DATA:", $user['username'], "PHONE DATA:", $user['username']);
	printf("--------------------------------------------------------------------------------\n");
	printf("%11s %-26s | %11s %-26s\n", "Timestamp:", $last_data['unixtime'],  "Timestamp:", $phone_data['unixtime']);
	printf("%11s %-26s | %11s %-26s\n", "DateTime:",  $last_data['timestamp'], "DateTime:",  $phone_data['timestamp']);
	printf("%11s %-26s | %11s %-26s\n", "Latitude:",  $last_data['latitude'],  "Latitude:",  $phone_data['latitude']);
	printf("%11s %-26s | %11s %-26s\n", "Longitude:", $last_data['longitude'], "Longitude:", $phone_data['longitude']);
	printf("%11s %-26s | %11s %-26s\n", "Accuracy:",  $last_data['accuracy'],  "Accuracy:",  $phone_data['accuracy']);
	printf("%11s %-26s | ",             "Location:",  $last_data['location']);


	
	// If the date in the phone is newer than our last known location, then we have a new 
	// entry and will do something with it
	if ($phone_data['unixtime'] > $last_data['unixtime']) {
		# we have a new data -- check to see where we're at.
		$phone_data['location'] = _get_db_known_locations($user['id'], $phone_data);
		printf("%11s %-26s\n", "Location:", $phone_data['location']);
		printf("--------------------------------------------------------------------------------\n");
		printf("%11s %-26s | ", "", "NEW PHONE DATA!");
					

		// Check to see if we're in the same location, but just have an updated entry
		if ($phone_data['location'] == $last_data['location']) {
			// same place, just update the log with the new timestamp
			_update_db($user['id'], $phone_data);
			printf("%-38s\n\n", "No change.  Still at: ".$phone_data['location']);
		}
		// We've moved!  do some checking to see where we're at
		else {
			// If the new location is unknown we'll say we're traveling.
			if ($phone_data['location'] == "unknown") { $phone_data['location'] = "traveling"; }
			
			// check to see if phone is in an unknown location, but in same spot.  
			// If we're in an unknown location (traveling), but still in the same spot we're "lost"
			if (_spot_check($last_data, $phone_data) && $phone_data['location'] == "traveling" &&
			   ($last_data['location'] == "traveling" || $last_data['location'] == "lost")) {
				$phone_data['location'] = "lost";
			}
			
			// Update the DB with the location
			_update_db($user['id'], $phone_data);
			printf("%-38s\n\n", "Moved!  New loc: ".$phone_data['location']);
		}
	}
		
	// No new phone data -- NOTE:  Need to check if this logic works.
	else {
		printf("\n%11s %-26s | ", "", "NO NEW DATA!");
		// If the timestamp is older than our mortalperil timeout, update us to mortalperil.
		if ($phone_data['unixtime'] < $current_timestamp - $mortalperil_timeout) {
			$phone_data['location']  = 'mortalperil';
			$phone_data['timestamp'] = $current_datetime;
			$phone_data['latitude']  = 00.0000;
			$phone_data['longitude'] = 00.0000;
			$phone_data['accuracy']  = 0;
			_update_db($user['id'], $phone_data);
			printf("%-38s\n\n", "Timeout reached: MORTAL PERIL!");
		}
		else {
			printf("%-38s\n\n", "No new DB entry");
		}
	}
}

// Delete old entries
_expire_old_history();

// Close the DB
mysql_close($db);

?>
