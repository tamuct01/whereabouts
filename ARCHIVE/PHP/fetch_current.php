<?PHP
// Description:
// This script is called by the Arduino itself and is used to get only the
// current locations for clock users.
//
//
// Global Definitions
//
// Database connection information
$db_hostname = "localhost";
$db_username = "whereabouts";
$db_password = "password";
$db_database = "whereabouts";

// Connect to the DB
$db = mysqli_connect($db_hostname, $db_username, $db_password, $db_database)
 	or die("Could not connect to DB: ".mysqli_error());





// FUNCTIONS


// Function to get the data from the user table
function _get_users () {
	global $db;

	$query = "SELECT * FROM user";
	$results = array();
	$q_result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	while ($results_row = mysql_fetch_array($q_result)) {
		array_push($results, $results_row);
	}

	return $results;
}



// Function to get the latest location entry from the DB for a specific user
function _get_last_db_entry ($uid) {
	global $db;

	$query = 'SELECT h.timestamp, cl.name AS location
FROM `history` `h`
INNER JOIN `clock_location` `cl`
	ON `h`.`clock_location_id` = `cl`.`id`
WHERE `h`.`user_id` = \''.$uid.'\'
ORDER BY h.timestamp DESC
LIMIT 1';

	$result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	$results_arr = mysql_fetch_array($result);

	return $results_arr['location'];
}

// Function to get the latest location entry from the DB for a specific user
function _get_clock_location ($loc_id) {
	global $db;

	$query = 'SELECT name AS location
FROM `clock_location`
WHERE `id` = '.$loc_id;

	$result = mysql_query($query, $db) or die("Could not query DB for clock location by ID entry: \n".mysql_error());
	$results_arr = mysql_fetch_array($result);

	return $results_arr['location'];
}





// -------- MAIN ---------------
$users = _get_users();

// Prints some basic XML data that the Arduino can parse
print '<?xml version="1.0" encoding="ISO-8859-1"?>
<whereabouts>
';

// Loop through each user
foreach ($users as $user) {
	/*
	 / Look at the location_source field and determine what to do
	 / For GPS, get the GPS data from the phone
	 / For clock_<ID> the user's location has been overridden in the DB to a
	 /   specific location.
	 / For user_<ID> the user's location is tied to another user's location.
	 /   This is useful when children or other family members don't have GPS.
	 */
	$username = $user['username'];
	$hand_id  = $user['hand_id'];
	if ($user['location_source'] == 'GPS') {
		$location = _get_last_db_entry($user['id']);
		print "<$username>$location</$username>\n";
	}
	elseif (preg_match("/^(clock|user)_(\d+)$/", $user['location_source'], $matches)) {
		//print_r($matches);
		// If the user is set to use another user's location, grab that user's last location
		if ($matches[1] == 'user') {
			$location = _get_last_db_entry($matches[2]);
			print "<$username>$location</$username>\n";
		}
		elseif ($matches[1] == 'clock') {
			$location = _get_clock_location($matches[2]);
			print "<$username>$location</$username>\n";
		}
		else {
			print("ERROR in specified clock location or user ID\n\n");
		}
	}
	else {
		print("ERROR in user location source\n\n");
	}
}

print "</whereabouts>\n";


// Close the DB
mysql_close($db);

?>
