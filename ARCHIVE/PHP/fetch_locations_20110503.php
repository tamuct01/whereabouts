<?PHP
require 'class.sosumi.php';

// Enter your MobileMe username and password
$ssm = new Sosumi('your.email@here.com', 'XXXXXXXXX');
$users = array("dad" => 0, "mom" => 1, "child1" => "", "child2" => "", "child3" => "");

/* Calculation factors for converting accuracy of GPS coordinates from meters to degrees of lat/long.
	The latitude value is consistent, but the longitude value changes depending on the degree of latitude you are at.
	Austin, TX is pretty close to 30.4 deg. latitude, and this is what the calculation is based on.
	These numbers were calculated from http://www.csgnetwork.com/degreelenllavcalc.html */
$deg_lat_to_meter = 110859.193020;
$deg_long_to_meter = 96096.951354;

// timeout values (in sec) for mortal peril
// 24 hours
$mortalperil_timeout = 86400;

// Database connection information
$db_hostname = "localhost";
$db_username = "whereabouts";
$db_password = "XXXXXXX";
$db_database = "whereabouts";

// Timestamp and datatime data for right now
$current_timestamp = time();
$current_datetime = strftime("%F %T", $current_timestamp);
print "CURRENT:\nTimestamp: $current_timestamp\nDateTime: $current_datetime\n\n";


// Function to get the latest DB entry for a specific user
function _get_last_db_entry ($user) {
	global $db_hostname, $db_username, $db_password, $db_database;
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "SELECT * FROM `$user` ORDER BY timestamp DESC LIMIT 1";
	$result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	$results_arr = mysql_fetch_array($result);

	mysql_close($db);
	return $results_arr;
}


// Function to get the location data from the DB based on user, lat, long, and accuracy
function _get_db_known_locations ($user, $lat, $long, $acc) {
	global $deg_lat_to_meter, $deg_long_to_meter, $db_hostname, $db_username, $db_password, $db_database;

	// if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	if ($acc < 30) { $acc = 30; }

	// Convert the accuracy to degrees
	$lat_acc = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min = $lat - $lat_acc;
	$lat_max = $lat + $lat_acc;
	$long_min = $long - $long_acc;
	$long_max = $long + $long_acc;
	
	// And search for locations that fall within the box
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "SELECT * FROM `Locations` WHERE (`latitude` BETWEEN ".$lat_min." AND ".$lat_max." AND `longitude` BETWEEN ".$long_min." AND ".$long_max.") AND (`validusers` = 'ALL' OR `validusers` LIKE '%".$user."%')";
	$result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

	if (mysql_num_rows($result) > 0) {
		$results_arr = mysql_fetch_array($result);
		mysql_close($db);
		return $results_arr['name'];		
	}
	else {
		mysql_close($db);
		return "unknown";
	}
}


// Update the DB with the latest phone data
function _update_db ($user, $logmessage) {
	global $db_hostname, $db_username, $db_password, $db_database;
	list($datetime, $lat, $long, $acc, $location) = explode('|', $logmessage);

	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "INSERT INTO $user (`timestamp`, `latitude`, `longitude`, `accuracy`, `location`) VALUES ('$datetime', '$lat', '$long', '$acc', '$location')";
	mysql_query($query, $db) or die("Could not update DB: ".mysql_error());
	mysql_close($db);
}


// Function to check to see if the new phone data is near the most recent location
function _spot_check($last_loc, $phone_loc) {
	global $deg_lat_to_meter, $deg_long_to_meter;

	// if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	$acc = $phone_loc['accuracy'];
	if ($acc < 30) { $acc = 30; }
	
	// Convert the accuracy to degrees
	$lat_acc = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min  = $phone_loc['latitude']  - $lat_acc;
	$lat_max  = $phone_loc['latitude']  + $lat_acc;
	$long_min = $phone_loc['longitude'] - $long_acc;
	$long_max = $phone_loc['longitude'] + $long_acc;
	
	if ($last_loc['latitude'] >= $lat_min && $last_loc['latitude'] <= $lat_max && $last_loc['longitude'] >= $long_min && $last_loc['longitude'] <= $long_max) {
		return true;
	}
	else {
		return false;
	}
}


// Function to get a location based on a schedule
function _get_scheduled_location($user, $current_timestamp) {
	global $db_hostname, $db_username, $db_password, $db_database;
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());
	
	$day = date("l", $current_timestamp);
	$time = date("H:i:s", $current_timestamp);
	
	$query = "SELECT `location` FROM `Schedules` WHERE `user` = '".$user."' AND `day` = '".$day."' AND '".$time."' BETWEEN `starttime` AND `endtime`";
	$result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

	if (mysql_num_rows($result) > 0) {
		$results_arr = mysql_fetch_array($result);
		return $results_arr['location'];		
	}
	else {
		return "NONE";
	}
}
	
	



// -------- MAIN ---------------
while (list($user, $uid) = each($users)) {
	$last_dat = _get_last_db_entry($user);
	//print "$last_dat\n";
	$last_datetime = $last_dat['timestamp'];
	$last_timestamp = strtotime($last_datetime);
	$last_lat      = $last_dat['latitude'];
	$last_long     = $last_dat['longitude'];
	$last_acc      = $last_dat['accuracy'];
	$last_location = $last_dat['location'];

	print "LAST DATA: $user\nTimestamp: $last_timestamp\nDateTime: $last_datetime\nLatitude: $last_lat\nLongitude: $last_long\nAccuracy: $last_acc\nLocation: $last_location\n\n";

	// If the UID is an integer (a person with an iPhone)
	if (is_int($uid)) {
		// Get the phone's location from the MobileMe site
		$loc = $ssm->locate($uid);
		$phone_datetime = $loc['timestamp'];
		$phone_timestamp = strtotime($phone_datetime);
		$phone_lat = $loc['latitude'];
		$phone_long = $loc['longitude'];
		$phone_acc = $loc['accuracy'];
		$logmessage = "$phone_datetime|$phone_lat|$phone_long|$phone_acc";

		print "PHONE DATA: $user\nTimestamp: $phone_timestamp\nDateTime: $phone_datetime\nLatitude: $phone_lat\nLongitude $phone_long\nAccuracy: $phone_acc\n";

		
		// If the date in the phone is newer than our last known location, then we have a new log entry and will do something
		if ($phone_timestamp > $last_timestamp) {
			# we have a new data -- check to see where we're at.
			print "New data!\n";
			$phone_location = _get_db_known_locations($user, $phone_lat, $phone_long, $phone_acc);
			print "Location: $phone_location\n";

			// Check to see if we're in the same location, but just have an updated entry
			if ($phone_location == $last_location) {
				$logmessage .= "|$phone_location";
				// same place, just update the log with the new timestamp
				_update_db($user, $logmessage);
				print "$phone_datetime -- $user is still:  $phone_location\n\n";
			}
			else {
				// We've moved!  do some checking to see where we're at
				if ($phone_location == "unknown") {
					$phone_location = "traveling";
				}
				
				// check to see if phone is in an unknown location, but in same spot.  If so, we're "lost"
				if (_spot_check($last_dat, $loc) && $last_dat['location'] == "traveling") {
					$phone_location = "lost";
				}
				
				$logmessage .= "|$phone_location";
				_update_db($user, $logmessage);
				print "$phone_datetime -- $user is now:  $phone_location\n\n";
			}
		}
		else {
			print "no new data\n";
			# Check our time for timeout and lost or mortal peril status
			if ($phone_timestamp < $current_timestamp - $mortalperil_timeout) {
				$logmessage .= "|moralperil";
				_update_db($user, $logmessage);
				print "$phone_datetime -- $user is in MORTAL PERIL\n\n";
			}
			else {
				print "No new log entry\n\n";
			}
		}
	}

	else {
		// For the kids, we can schedule some of the items.
		$location = _get_scheduled_location($user, $current_timestamp);
		
		if ($location != "NONE") {
			$lat = 0;
			$long = 0;
			$acc = 0;
			print "CURR DATA: $user\nTimestamp: $current_timestamp\nDateTime: $current_datetime\nLatitude: $lat\nLongitude $long\nAccuracy: $acc\nLocation: $location\n";
			
			$logmessage = "$current_datetime|$lat|$long|$acc|$location";
			_update_db($user, $logmessage);
			
			print "$datetime -- $user is at $location\n\n";
		}
		else {
			// Just map the kids to mom for now.
			// Should be able to add usernames to the override table to make the kids attach to someone else.
			$dat = _get_last_db_entry("mom");
			$datetime = $dat['timestamp'];
			$timestamp = strtotime($datetime);
			$lat      = $dat['latitude'];
			$long     = $dat['longitude'];
			$acc      = $dat['accuracy'];
			$location = $dat['location'];

			print "CURR DATA: $user\nTimestamp: $timestamp\nDateTime: $datetime\nLatitude: $lat\nLongitude $long\nAccuracy: $acc\nLocation: $location\n";

			$logmessage = "$datetime|$lat|$long|$acc|$location";
			_update_db($user, $logmessage);

			print "$datetime -- $user is at $location\n\n";
		}
	}
}

?>