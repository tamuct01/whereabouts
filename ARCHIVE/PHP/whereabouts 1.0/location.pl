#!/usr/bin/perl

use warnings;
use strict;
use POSIX qw(strftime);
use Date::Parse;


# Calculation factors for converting accuracy of GPS coordinates from meters to degrees of lat/long.
# The latitude value is consistent, but the longitude value changes depending on the degree of latitude you are at.
# Austin, TX is pretty close to 30.4 deg. latitude, and this is what the calculation is based on.
# These numbers were calculated from http://www.csgnetwork.com/degreelenllavcalc.html
my $deg_lat_to_meter = 110859.193020;
my $deg_long_to_meter = 96096.951354;

# timeout values (in sec) for lost and mortal peril
# 2 hours
my $lost_timeout = 7200;
# 24 hours
my $mortalperil_timeout = 86400;

# The file that contains the last update to location
my $last_locations_file = "/var/www/location_tmp/last_locations.txt";


# List of users
my %users = (
	dad => "dad",
	mom => "Mom",
	child1 => "Child1",
	child2 => "Child2",
	child3 => "Child3"
);

# List of locations.  Not sure if I want to tie this to a particular user or not.  My work might be different than mom's, etc.
my %locations = (
	work => {
		name => "work",
		grammar => "is at",
		lat => 30.xxxxx,
		long => -97.xxxxxx
	},
	home => {
		name => "home",
		grammar => "is at",
		lat => 30.xxxxx,
		long => -97.xxxxx
	},
	church => {
		name => "church",
		grammar => "is at",
		lat => 30.xxxxx,
		long => -97.xxxxx
	},
	lost => {
		name => "lost",
		grammar => "is",
		lat => 0,
		long => 0
	},
	mortalperil => {
		name => "mortal peril",
		grammar => "is in",
		lat => 0,
		long => 0
	},
	traveling => {
		name => "traveling",
		grammar => "is",
		lat => 0,
		long => 0
	}
);



sub _get_known_locations ($$$) {
	my ($lat, $long, $acc) = @_;
	# if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	if ($acc < 30) { $acc = 30; }

	my $long_acc = $acc / $deg_long_to_meter;
	my $lat_acc = $acc / $deg_lat_to_meter;

	foreach my $loc (keys(%locations)) {
		my $loc_lat = $locations{$loc}{lat};
		my $loc_long = $locations{$loc}{long};

		my $loc_lat_min = $loc_lat - $lat_acc;
		my $loc_lat_max = $loc_lat + $lat_acc;
		my $loc_long_min = $loc_long - $long_acc;
		my $loc_long_max = $loc_long + $long_acc;

		if ($lat >= $loc_lat_min && $lat <= $loc_lat_max && $long >= $loc_long_min && $long <= $loc_long_max) {
			return $loc;
		}
	}
	return "unknown";
}

sub _get_last_location ($) {
	my $user = shift;

	# Get the timestamp and location the user last was at
	open(LASTLOCATION, "<$last_locations_file") or die ("Could not open last_location file for reading - $!");
	while (<LASTLOCATION>) {
		my ($timestamp, $uid, $loc) = split(',', $_);
		if ($uid eq $user) {
			return ($timestamp, $loc);
		}
	}
	close(LASTLOCATION);
}


sub _get_last_entry ($) {
	my $user = shift;

	# The location file we want to read
	my $locations_file = "/var/www/location_tmp/locations_${user}.txt";

	open(LOCATION, "<$locations_file") or die ("Could not open location file - $!");
	my $line;
	while (<LOCATION>) {
		if (m/^#/) { next; }
		else { chomp($line = $_); }
	}
	close(LOCATION);

	return $line;
}


sub _update_log ($$$) {
	my ($user, $timestamp, $location) = @_;

	# Update the timestamp and location the user last was at
	open(LASTLOCATION, ">$last_locations_file") or die ("Could not open last_location file for update - $!");
	print LASTLOCATION "$timestamp,$user,$location";
	close(LASTLOCATION);
}

sub _alert_mythtv ($$) {
	my ($user, $loc) = @_;
	my $alert_text = $users{$user}." ".$locations{$loc}{grammar}." ".$locations{$loc}{name};
	print "$alert_text\n";

	my $mythtvosd_cmd = "/usr/bin/mythtvosd --template=alert --alert_text=\"$alert_text\"";
	system($mythtvosd_cmd) == 0 or die("system $mythtvosd_cmd failed $?");
}


## MAIN
# This is temporary until I add more users to the processing
my $user = "dad";

# Get the timestamp and location the user last was at
my ($last_timestamp, $last_location) = _get_last_location($user);

# The current time in timestamp notation
my $curr_time = time();
# my $curr_datetime = strftime("%Y-%m-%d %H:%M:%S", localtime);

# Get the last entry from the logfile that is updated by the php script
my $last_entry = _get_last_entry($user);

# Split it out into component pieces
my ($datetime, $lat, $long, $acc, $alt, $altacc, $batt, $ip) = split(/\|/, $last_entry);
# Convert to Unix timestamp
$datetime = str2time($datetime);


# If the date in the PHP log is newer than our last known location, then we have a new log entry and will do something
if ($datetime > $last_timestamp) {
	# we have a new log entry -- check to see where we're at.
	my $curr_location = _get_known_locations($lat, $long, $acc);

	# Check to see if we're in the same location, but just have an updated entry
	if ($curr_location eq $last_location) {
		# same place, just update the log with the new timestamp
		_update_log($user, $datetime, $curr_location);
		print "$datetime -- dad is still at:  $curr_location\n";
	}
	else {
		# We've moved!  do some checking to see where we're at
		if ($last_location ne "unknown" and $curr_location eq "unknown") {
			$curr_location = "traveling";
		}
		_update_log($user, $datetime, $curr_location);
		_alert_mythtv($user, $curr_location);
	}
}
else {
	# Check our time for timeout and lost or mortal peril status
	if ($datetime < $curr_time - $mortalperil_timeout) {
		_update_log($user, $curr_time, "mortalperil");
		_alert_mythtv($user, "mortalperil");
# 		print "$datetime -- $user is in MORTAL PERIL\n";
	}
	elsif ($datetime < $curr_time - $lost_timeout) {
		_update_log($user, $curr_time, "lost");
		_alert_mythtv($user, "lost");
# 		print "$datetime -- $user is LOST\n";
	}
	else {
		print "No new log entry\n";
	}
}



exit 0;
1;
