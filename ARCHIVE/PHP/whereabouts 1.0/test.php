<?PHP
require 'class.sosumi.php';

// Enter your MobileMe username and password
$ssm = new Sosumi('your.email@here.com', 'XXXXXXXXX');
$users = array("dad" => 0, "mom" => 1);



//List of locations.  Not sure if I want to tie this to a particular user or not.  My work might be different than mom's, etc.
$locations = array (
	"work" => array(
		"name" => "work",
		"grammar" => "is at",
		"lat" => 30.401399,
		"long" => -97.72390
	),
	"home" => array(
		"name" => "home",
		"grammar" => "is at",
		"lat" => 30.402588,
		"long" => -97.648008
	),
	"church" => array(
		"name" => "church",
		"grammar" => "is at",
		"lat" => 30.516474,
		"long" => -97.683488
	),
	"lost" => array(
		"name" => "lost",
		"grammar" => "is",
		"lat" => 0,
		"long" => 0
	),
	"mortalperil" => array(
		"name" => "mortal peril",
		"grammar" => "is in",
		"lat" => 0,
		"long" => 0
	),
	"traveling" => array(
		"name" => "traveling",
		"grammar" => "is",
		"lat" => 0,
		"long" => 0
	)
);

/* Calculation factors for converting accuracy of GPS coordinates from meters to degrees of lat/long.
	The latitude value is consistent, but the longitude value changes depending on the degree of latitude you are at.
	Austin, TX is pretty close to 30.4 deg. latitude, and this is what the calculation is based on.
	These numbers were calculated from http://www.csgnetwork.com/degreelenllavcalc.html */
$deg_lat_to_meter = 110859.193020;
$deg_long_to_meter = 96096.951354;

// timeout values (in sec) for lost and mortal peril
// 2 hours
$lost_timeout = 7200;
// 24 hours
$mortalperil_timeout = 86400;


// Timestamp and datatime data for right now
$current_timestamp = time();
$current_datetime = strftime("%F %T", $current_timestamp);
print "CURRENT:\nTimestamp: $current_timestamp\nDateTime: $current_datetime\n\n";



// Read the last line(s) from the logfile using Unix tail
function readLastLines($filename, $lines){
	$cmd = "tail -$lines $filename";
	$output = shell_exec($cmd);
	return chop($output);
}


// Get the known locations from the lat/long data supplied
function _get_known_locations ($lat, $long, $acc) {
	global $deg_lat_to_meter, $deg_long_to_meter, $locations;
	// if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	if ($acc < 30) { $acc = 30; }

	$long_acc = $acc / $deg_long_to_meter;
	$lat_acc = $acc / $deg_lat_to_meter;

	foreach ($locations as $location) {
		$loc_lat = $location['lat'];
		$loc_long = $location['long'];

		$loc_lat_min = $loc_lat - $lat_acc;
		$loc_lat_max = $loc_lat + $lat_acc;
		$loc_long_min = $loc_long - $long_acc;
		$loc_long_max = $loc_long + $long_acc;

		if ($lat >= $loc_lat_min && $lat <= $loc_lat_max && $long >= $loc_long_min && $long <= $loc_long_max) {
			return $location['name'];
		}
	}
	return "unknown";
}


// Update the log with new info
function _update_log ($user, $logmessage) {
	// Update log with the location data
	$myFile = "locations_".$user.".txt";
	$fh = fopen($myFile, 'a') or die("can't open output file");
	fwrite($fh, $logmessage);
	fclose($fh);
}


// Send an alert to mythtv
function _alert_mythtv ($user, $loc) {
	global $locations;
	$alert_text = $user." ".$locations[$loc]['grammar']." ".$locations[$loc]['name'];
	print "$alert_text\n";

	// MythTV OSD is broken in 0.24
	// $mythtvosd_cmd = "/usr/bin/mythtvosd --template=alert --alert_text=\"$alert_text\"";
	// shell_exec($mythtvosd);
}


function _update_db ($user, $logmessage) {



$db = mysql_connect('localhost', 'whereabouts', 'XXXXXXX');
		mysql_select_db('whereabouts', $db) or die(mysql_error());
		
		$query = "INSERT INTO DAD (`timestamp`, `latitude`, `longitude`, `accuracy`, `location`) VALUES (\'$loc['timestamp']\', \'$loc['latitude']\', \'$loc['longitude']\', $loc['accuracy'], $phone_location)";
		mysql_query($query, $db) or die(mysql_error());

}




// -------- MAIN ---------------
while (list($user, $uid) = each($users)) {
	# Get the timestamp and location the user last was at
	$last_location_data = readLastLines("locations_".$user.".txt", 1);
	print "$last_location_data\n";
	list($last_datetime, $last_lat, $last_long, $last_acc, $last_location) = explode('|', $last_location_data);
	$last_timestamp = strtotime($last_datetime);

	print "LAST DATA:\nTimestamp: $last_timestamp\nDateTime: $last_datetime\nLatitude: $last_lat\nLongitude: $last_long\nAccuracy: $last_acc\nLocation: $last_location\n\n";


	// Get the phone's location from the MobileMe site
	$loc = $ssm->locate($uid);
	$phone_timestamp = strtotime($loc['timestamp']);
	$phone_datetime = $loc['timestamp'];
	$phone_lat = $loc['latitude'];
	$phone_long = $loc['longitude'];
	$phone_acc = $loc['accuracy'];
	$logmessage = "$phone_datetime|$phone_lat|$phone_long|$phone_acc";

	print "PHONE DATA:\nTimestamp: $phone_timestamp\nDateTime: $phone_datetime\nLatitude: $phone_lat\nLongitude $phone_long\nAccuracy: $phone_acc\n";


	// If the date in the phone is newer than our last known location, then we have a new log entry and will do something
	if ($phone_timestamp > $last_timestamp) {
		# we have a new log entry -- check to see where we're at.
		print "New data!\n";
		$phone_location = _get_known_locations($phone_lat, $phone_long, $phone_acc);
		print "Location: $phone_location\n\n";

		// Check to see if we're in the same location, but just have an updated entry
		if ($phone_location == $last_location) {
			$logmessage .= "|$phone_location\n";
			// same place, just update the log with the new timestamp
			_update_log($user, $logmessage);
			_update_db($user, $logmessage);
			print "$phone_datetime -- $user is still at:  $phone_location\n";
		}
		else {
			// We've moved!  do some checking to see where we're at
			if ($last_location != "unknown" and $phone_location == "unknown") {
				$phone_location = "traveling";
			}
			$logmessage .= "|$phone_location\n";
			_update_log($user, $logmessage);
			_alert_mythtv($user, $phone_location);
			_update_db($user, $logmessage);
			print "$phone_datetime -- $user is now at:  $phone_location\n";
		}
	}
	else {
		print "no new data\n";
		# Check our time for timeout and lost or mortal peril status
		if ($phone_timestamp < $current_timestamp - $mortalperil_timeout) {
			$logmessage .= "|moralperil\n";
			_update_log($user, $logmessage);
			_alert_mythtv($user, "mortalperil");
			_update_db($user, $logmessage);
			print "$phone_datetime -- $user is in MORTAL PERIL\n";
		}
		elseif ($phone_timestamp < $current_timestamp - $lost_timeout) {
			$logmessage .= "|lost\n";
 			_update_log($user, $logmessage);
 			_alert_mythtv($user, "lost");
			_update_db($user, $logmessage);
 	 		print "$phone_datetime -- $user is LOST\n";
		}
		else {
			print "No new log entry\n";
		}
	}
}
?>
