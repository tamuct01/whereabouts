<?PHP
require 'class.sosumi.php';
// Description:
// TBD !
//
//
// Global Definitions
// Change these for different users

// Enter your iCloud username and password
$ssm = new Sosumi('your.email@here.com', 'XXXXXXXXX');

// User array defined as "Name' => iCloud idevice ID
$users = array("dad" => 3, "mom" => 1, "child1" => "", "child2" => "", "child3" => "");

// Database connection information
$db_hostname = "localhost";
$db_username = "whereabouts";
$db_password = "XXXXXXX";
$db_database = "whereabouts";

// Don't change the rest, please!!
// Additional globals

// Minimum accuracy value.  If the phone is really accurate it can throw off the 
// reading since the location is just a point value.
$min_accuracy = 50;

// Timeout value (in sec) to trigger mortal peril
// If the last phone data is older than this, then the user is in mortal peril.
$mortalperil_timeout = 7200;

/*	
	Calculation factors for converting accuracy of GPS coordinates from meters to degrees of 
	lat/long.  The latitude value is consistent, but the longitude value changes depending on the 
	degree of latitude you are at.	Austin, TX is pretty close to 30.4 deg. latitude, and this is
	what the calculation is based on.  These numbers were calculated from
	http://www.csgnetwork.com/degreelenllavcalc.html
*/
$deg_lat_to_meter = 110859.193020;
$deg_long_to_meter = 96096.951354;


// Timestamp and datatime data for right now
$current_timestamp = time();
$current_datetime = strftime("%F %T", $current_timestamp);
print "CURRENT TIMESTAMPS:\n";
print "Timestamp: $current_timestamp\n";
print "DateTime: $current_datetime\n\n";


// FUNCTIONS


// Function to get the latest location entry from the DB for a specific user
function _get_last_db_entry ($user) {
	global $db_hostname, $db_username, $db_password, $db_database;
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "SELECT * FROM `$user` ORDER BY timestamp DESC LIMIT 1";
	$result = mysql_query($query, $db) or die("Could not query DB for last entry: ".mysql_error());
	$results_arr = mysql_fetch_array($result);

	mysql_close($db);
	return $results_arr;
}



// Function to get the location data from the DB based on user, lat, long, and accuracy
function _get_db_known_locations ($user, $lat, $long, $acc) {
	global $deg_lat_to_meter, $deg_long_to_meter, $db_hostname, $db_username, $db_password, $db_database, $min_accuracy;

	// if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	if ($acc < $min_accuracy) { $acc = $min_accuracy; }

	// Convert the accuracy to degrees
	$lat_acc = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min = $lat - $lat_acc;
	$lat_max = $lat + $lat_acc;
	$long_min = $long - $long_acc;
	$long_max = $long + $long_acc;
	
	// And search for locations in the DB that fall within the bounding box
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "SELECT * FROM `Locations` WHERE (`latitude` BETWEEN ".$lat_min." AND ".$lat_max." AND `longitude` BETWEEN ".$long_min." AND ".$long_max.") AND (`validusers` = 'ALL' OR `validusers` LIKE '%".$user."%')";
	$result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

	if (mysql_num_rows($result) > 0) {
		$results_arr = mysql_fetch_array($result);
		mysql_close($db);
		return $results_arr['name'];		
	}
	else {
		mysql_close($db);
		return "unknown";
	}
}




// Update the DB with the latest phone data from a user
function _update_db ($user, $data) {
	global $db_hostname, $db_username, $db_password, $db_database;
	list($datetime, $lat, $long, $acc, $location) = explode('|', $data);

	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

	$query = "INSERT INTO $user (`timestamp`, `latitude`, `longitude`, `accuracy`, `location`) VALUES ('$datetime', '$lat', '$long', '$acc', '$location')";
	mysql_query($query, $db) or die("Could not update DB: ".mysql_error());
	mysql_close($db);
}




// Function to check to see if the new phone data is near the most recent location
function _spot_check($last_loc, $phone_loc) {
	global $deg_lat_to_meter, $deg_long_to_meter, $min_accuracy;

	// if the accuracy reporting from the phone is too small it can generate false reports.  Here we'll adjust the accuracy to a good minimum
	$acc = $phone_loc['accuracy'];
	if ($acc < $min_accuracy) { $acc = $min_accuracy; }
	
	// Convert the accuracy to degrees
	$lat_acc = $acc / $deg_lat_to_meter;
	$long_acc = $acc / $deg_long_to_meter;
	
	// Create a bounding box around the phone location	
	$lat_min  = $phone_loc['latitude']  - $lat_acc;
	$lat_max  = $phone_loc['latitude']  + $lat_acc;
	$long_min = $phone_loc['longitude'] - $long_acc;
	$long_max = $phone_loc['longitude'] + $long_acc;
	
	if ($last_loc['latitude'] >= $lat_min && $last_loc['latitude'] <= $lat_max && $last_loc['longitude'] >= $long_min && $last_loc['longitude'] <= $long_max) {
		# the latest phone data was near the last data (i.e. the user is in the same place)
		return true;
	}
	else {
		# the user has moved
		return false;
	}
}




// Function to get a location based on a schedule
// NOTE - I'd like to update this to work with a google calendar!
function _get_scheduled_location($user, $current_timestamp) {
	global $db_hostname, $db_username, $db_password, $db_database;
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());
	
	$day = date("l", $current_timestamp);
	$time = date("H:i:s", $current_timestamp);
	
	$query = "SELECT `location` FROM `Schedules` WHERE `user` = '".$user."' AND `day` = '".$day."' AND '".$time."' BETWEEN `starttime` AND `endtime`";
	$result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

	if (mysql_num_rows($result) > 0) {
		$results_arr = mysql_fetch_array($result);
		return $results_arr['location'];		
	}
	else {
		return "NONE";
	}
}



// Function to get the Override status of a user
// There is a table in the DB that controls the overriding location of the user.
// If set to 'GPS', then GPS coordinates will be used to get the location.
// If set to another user - the user's location will be updated to the user listed
// If set to a location, that location will be used instead of anything else.
function _get_override($user) {
	global $db_hostname, $db_username, $db_password, $db_database;
	$db = mysql_connect($db_hostname, $db_username, $db_password);
	mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());
	
	$query = "SELECT `location` FROM `Override` WHERE `user` = '".$user."'";
	$result = mysql_query($query, $db) or die("Could not query DB for override data: ".mysql_error());
	$results_arr = mysql_fetch_array($result);
	
	return trim($results_arr['location']);
}	
	
	





// -------- MAIN ---------------
// Loop through each user
while (list($user, $uid) = each($users)) {
	// Populate the previous data to compare against.
	$last_dat = _get_last_db_entry($user);
	$last_datetime = $last_dat['timestamp'];
	$last_timestamp = strtotime($last_datetime);
	$last_lat      = $last_dat['latitude'];
	$last_long     = $last_dat['longitude'];
	$last_acc      = $last_dat['accuracy'];
	$last_location = $last_dat['location'];
	// print "LAST DATA: $user\nTimestamp: $last_timestamp\nDateTime: $last_datetime\nLatitude: $last_lat\nLongitude: $last_long\nAccuracy: $last_acc\nLocation: $last_location\n\n";

	// If the UID is an integer (a person with an iPhone) do a bunch of stuff
	// At this point, the script will only do GPS data for iphone users.  In the Arduino script, 
	// The Override table comes into play.
	if (is_int($uid)) {
		// Get the phone's location from the iCloud site
		$loc = $ssm->locate($uid);
		$phone_datetime = $loc['timestamp'];
		$phone_timestamp = strtotime($phone_datetime);
		$phone_lat = $loc['latitude'];
		$phone_long = $loc['longitude'];
		$phone_acc = $loc['accuracy'];
		$logmessage = "$phone_datetime|$phone_lat|$phone_long|$phone_acc";
		
		// Print a comparision of last and current data
		printf("%-11s %-26s | %-11s %-26s\n", "LAST DATA:", $user, "PHONE DATA:", $user);
		printf("%-11s %-26s | %-11s %-26s\n", "Timestamp:", $last_timestamp, "Timestamp:", $phone_timestamp);
		printf("%-11s %-26s | %-11s %-26s\n", "DateTime:",  $last_datetime, "DateTime:", $phone_datetime);
		printf("%-11s %-26s | %-11s %-26s\n", "Latitude:",  $last_lat, "Latitude:", $phone_lat);
		printf("%-11s %-26s | %-11s %-26s\n", "Longitude:", $last_long, "Longitude:", $phone_long);
		printf("%-11s %-26s | %-11s %-26s\n", "Accuracy:",  $last_acc, "Accuracy:", $phone_acc);
		printf("%-11s %-26s | ", "Location:", $last_location);
		
		// If the date in the phone is newer than our last known location, then we have a new 
		// entry and will do something with it
		if ($phone_timestamp > $last_timestamp) {
			# we have a new data -- check to see where we're at.
			$phone_location = _get_db_known_locations($user, $phone_lat, $phone_long, $phone_acc);
			printf("%-11s %-26s\n", "Location:", $phone_location);
			printf("%-38s | %-37s\n", "", "NEW PHONE DATA!");
			

			// Check to see if we're in the same location, but just have an updated entry
			if ($phone_location == $last_location) {
				$logmessage .= "|$phone_location";
				// same place, just update the log with the new timestamp
				_update_db($user, $logmessage);
				printf("%-38s | %-37s\n\n", "", "No change.  Still at: $phone_location");
			}
			else {
				// We've moved!  do some checking to see where we're at
				
				// If the new location is unknown we'll say we're traveling.
				if ($phone_location == "unknown") {
					$phone_location = "traveling";
				}
				
				// check to see if phone is in an unknown location, but in same spot.  
				// If we're unknown, but still in the same spot, we're "lost"
				if (_spot_check($last_dat, $loc) && $last_dat['location'] == "traveling" && $phone_location == "traveling") {
					$phone_location = "lost";
				}
				
				// Update the DB with the location
				$logmessage .= "|$phone_location";
				_update_db($user, $logmessage);
				printf("%-38s | %-37s\n\n", "", "Moved!  New loc: $phone_location");
			}
		}
		
		// No new phone data -- NOTE:  Need to check if this logic works.
		else {
			printf("%-11s %-26s\n", "Location:", "NO NEW DATA");
			// If the timestamp is older than our mortalperil timeout, update us to mortalperil.
			if ($phone_timestamp < $current_timestamp - $mortalperil_timeout) {
				$logmessage = "$current_datetime|00.0000|00.000|00|mortalperil";
				_update_db($user, $logmessage);
				printf("%-38s | %-37s\n\n", "", "Timeout reached: MORTAL PERIL!");
			}
			else {
				printf("%-38s | %-37s\n\n", "", "No new DB entry");
			}
		}
	}

	// If the userID is not an integer (no iPhone) do some other stuff
	else {
		// For the kids, we can schedule some of the items.
		// NOTE: I'd like to update this to use a google calendar instead of a dinky table entry.
		$location = _get_scheduled_location($user, $current_timestamp);
		
		// Print a comparision of last and current data
		printf("%-13s %-24s | %-13s %-24s\n", "LAST DATA:", $user, "CURRENT DATA:", $user);
		
		// If there was a scheduled location --
		if ($location != "NONE") {
			$lat = 0;
			$long = 0;
			$acc = 0;
			
			// Print a comparision of last and current data
			printf("%-13s %-24s | %-13s %-24s\n", "Timestamp:", $last_timestamp, "Timestamp:", $current_timestamp);
			printf("%-13s %-24s | %-13s %-24s\n", "DateTime:",  $last_datetime, "DateTime:", $current_datetime);
			printf("%-13s %-24s | %-13s %-24s\n", "Latitude:",  $last_lat, "Latitude:", $lat);
			printf("%-13s %-24s | %-13s %-24s\n", "Longitude:", $last_long, "Longitude:", $long);
			printf("%-13s %-24s | %-13s %-24s\n", "Accuracy:",  $last_acc, "Accuracy:", $acc);
			printf("%-13s %-24s | %-13s %-24s\n", "Location:", $last_location, "Location:", $location);
			
			$logmessage = "$current_datetime|$lat|$long|$acc|$location";
			_update_db($user, $logmessage);
			printf("%-38s | %-37s\n\n", "", "SCHEDULED");
		}
		else {
			// Get any override status that may be set.
			$override = _get_override($user);

			// If the override value is a user with an iPhone, then get the last data from the iPhone user			
			if (is_int($users[$override])) {
				$dat = _get_last_db_entry($override);
				$datetime = $dat['timestamp'];
				$timestamp = strtotime($datetime);
				$lat      = $dat['latitude'];
				$long     = $dat['longitude'];
				$acc      = $dat['accuracy'];
				$location = $dat['location'];

				// Print a comparision of last and current data
				printf("%-13s %-24s | %-13s %-24s\n", "Timestamp:", $last_timestamp, "Timestamp:", $timestamp);
				printf("%-13s %-24s | %-13s %-24s\n", "DateTime:",  $last_datetime, "DateTime:", $datetime);
				printf("%-13s %-24s | %-13s %-24s\n", "Latitude:",  $last_lat, "Latitude:", $lat);
				printf("%-13s %-24s | %-13s %-24s\n", "Longitude:", $last_long, "Longitude:", $long);
				printf("%-13s %-24s | %-13s %-24s\n", "Accuracy:",  $last_acc, "Accuracy:", $acc);
				printf("%-13s %-24s | %-13s %-24s\n", "Location:", $last_location, "Location:", $location);

				$logmessage = "$datetime|$lat|$long|$acc|$location";
				_update_db($user, $logmessage);
				printf("%-38s | %-37s\n\n", "", "$user --> $override");
			}
			// Some other case
			else {
				print "$user override is set as $override and I don't know what to do with it.";
			}
		}
	}
}

?>
