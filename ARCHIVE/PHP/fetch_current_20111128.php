<?PHP
$users = array("dad" => 0, "mom" => 1, "child1" => 2, "child2" => 3, "child3" => 4);

// Database connection information
$db_hostname = "localhost";
$db_username = "whereabouts";
$db_password = "XXXXXXX";
$db_database = "whereabouts";


function _get_last_db_entry ($user) {
        global $db_hostname, $db_username, $db_password, $db_database;
        $db = mysql_connect($db_hostname, $db_username, $db_password);
        mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());
	
	$query = "SELECT `location` FROM `Override` WHERE `user` = '".$user."'";
	$result = mysql_query($query, $db) or die("Could not query DB for override data: ".mysql_error());
	$results_arr = mysql_fetch_array($result);

	$schedule = _get_scheduled_location($user, time());
	
	// Get the data from the GPS table
	if ($results_arr['location'] == "GPS") {
		$query = "SELECT location FROM `$user` ORDER BY timestamp DESC LIMIT 1";
		$result = mysql_query($query, $db) or die("Could not query DB for latest location: ".mysql_error());
		$results_arr = mysql_fetch_array($result);
		return trim($results_arr['location']);
	}
	// Get scheduled locations
	elseif ($schedule != "NONE") {
		return $schedule;
	}
	// Get the data from another user's GPS location
	elseif ($results_arr['location'] == "mom" or $results_arr['location'] == "dad") {
		$query = "SELECT location FROM `".$results_arr['location']."` ORDER BY timestamp DESC LIMIT 1";
		$result = mysql_query($query, $db) or die("Could not query DB for latest location: ".mysql_error());
		$results_arr = mysql_fetch_array($result);
		return trim($results_arr['location']);
	}
	// Use the override location
	else {
		return trim($results_arr['location']);
	}
}



// Function to get a location based on a schedule
function _get_scheduled_location($user, $current_timestamp) {
        global $db_hostname, $db_username, $db_password, $db_database;
        $db = mysql_connect($db_hostname, $db_username, $db_password);
        mysql_select_db($db_database, $db) or die("Could not connect to DB: ".mysql_error());

        $day = date("l", $current_timestamp);
        $time = date("H:i:s", $current_timestamp);

        $query = "SELECT `location` FROM `Schedules` WHERE `user` = '".$user."' AND `day` = '".$day."' AND '".$time."' BETWEEN `starttime` AND `endtime`";
        $result = mysql_query($query, $db) or die("Could not query DB for location data: ".mysql_error());

        if (mysql_num_rows($result) > 0) {
                $results_arr = mysql_fetch_array($result);
                return $results_arr['location'];
        }
        else {
                return "NONE";
        }
}




// -------- MAIN ---------------
print '<?xml version="1.0" encoding="ISO-8859-1"?>
<whereabouts>
';

while (list($user, $uid) = each($users)) {
	$last_location = _get_last_db_entry($user);

	print "<$user>$last_location</$user>\n";
}

print "</whereabouts>\n";

?>
