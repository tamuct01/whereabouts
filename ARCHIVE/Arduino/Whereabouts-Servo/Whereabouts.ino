/*
Whereabouts Clock Arduino Code

This code will initialize the Arduino pins and then connect to the Wifi network specified.
It will fetch clock location data provided by the fetch_current.php script and parse
the JSON output in order to move the hand servos to the correct location on the clock face.
*/

// Include description files for other libraries used (if any)
#include <string.h>
#include <SPI.h>
#include <Servo.h>
#include <WiFi.h>
#include <ArduinoJson.h>


/* Network Information */
char ssid[] = "your_SSID";        // your network SSID (name) 
char pass[] = "your_WPA_pass";    // your network password


/* 
Some Notes on Arduino PIN usage
an * denotes those that are set by this Sketch and not default

Digital Pins:
  0 = Serial RX
  1 = Serial TX
  2
* 3 = Servo 1
  4 = Reserved for SD slot
* 5 = Servo 2
* 6 = Servo 3
  7 = Wifi Handshake
* 8 = Servo 4
* 9 = Servo 5
 10 = Ethernet/Wifi
 11 = Ethernet/Wifi
 12 = Ethernet/Wifi
 13 = Ethernet/Wifi

Analog Pins:
* 0 = Green "GOOD" LED
* 1 = Yellow "DATA" LED
* 2 = Red "ERROR" LED
* 3 = "Demo Mode" PIN - connected HIGH will randomly move hands
 4 = Used by Wifi Shield ?
 5 = Used by Wifi Shield ?
*/


// Define Constants
// Some constants/vars for LED data and timing
// LED pin assignments for status indicators
#define WIFI_LED A0
#define DATA_LED A1
#define ERR_LED  A2
#define DEMO_PIN A3

// WiFi/Ethernet vars
WiFiClient client;
IPAddress server(WEBSERVER_IP_ADDRESS);                    // Webserver IP address.  Comma separated (ex. 192, 168, 100, 10)
int Port = 80;                                             // Webserver port (usually 80)
static char Host[] = "Host: WEBSERVER_IP_ADDRESS";         // Webserver IP address.  (ex. 192.168.100.10)
static char Path[] = "GET /fetch_current.py HTTP/1.1";     // Path to current status script



// Servo control vars
// The number of people (or hands) that I have on the clock
#define NUM_PEOPLE 5
// Names of the people we track (must match what is returned from the server
const char* peopleNames[NUM_PEOPLE] = {"Dad", "Mom", "Child1", "Child2", "Child3"};
// The locations that we will fill from the server
const char* peopleLocations[NUM_PEOPLE] = {"", "", "", "", ""};
// The locations we know about that reference positions around the clock
// There are actually 18 locations the clock can physically reach, but we will only use the first 12
const char* Locations[] = { "home",        \
                            "quidditch",   \
                            "school",      \
                            "work",        \
                            "prison",      \
                            "traveling",   \
                            "church",      \
                            "lost",        \
                            "play",        \
                            "mortalperil", \
                            "holidays",    \
                            "hospital",    \
                            "home",        \
                            "quidditch",   \
                            "school",      \
                            "work",        \
                            "prison",      \
                            "traveling"};
// An array of servos for the people
Servo peopleServo [NUM_PEOPLE];
// mapping the servo to IO pins
byte peopleServoPin[NUM_PEOPLE] = {3, 5, 6, 8, 9};

// Min and max PWM signals for servos.  These are the operational recommendations from the servo manufacturer 900-2100
int servoMin[NUM_PEOPLE] = { 900, 1000,  900, 1000,  900};
int servoMax[NUM_PEOPLE] = {2050, 2100, 2080, 2100, 2125};
// offset value for each servo
byte servoOffset[NUM_PEOPLE] = {0, 0, 0, 0, 4};







// Setup 
void setup() {
  int status = WL_IDLE_STATUS;      // the Wifi radio's status

  // Setup the serial port
  Serial.begin(9600);
  
  // Initialize the LEDs.
  setupLed();
  
  // Print status to serial 
  Serial.println("Starting WiFi Ethernet and waiting to stabilize...");

  // check for the presence of the shield:
  while (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
    while (true); 
  }

//  String fv = WiFi.firmwareVersion();
//  Serial.print("WiFi firmware: ");
//  Serial.println(fv);
//  if (fv != "1.1.0") {
//    Serial.println("Please upgrade the firmware");
//    while(1);
//  }
  
  // Wait for a connection
  while (status != WL_CONNECTED) {
    // Attempt to connect to the Wifi network:
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
    status = WiFi.begin(ssid, pass);
 
    // wait 10 seconds for connection
    blinkLed(WIFI_LED, 1000, 10);
  }
  
  // Connected!
  Serial.println("Connected to WiFi");
  digitalWrite(WIFI_LED, HIGH);
  printWifiStatus();

  // Set client timeout
  client.setTimeout(10000);
}

void(* resetFunc) (void) = 0;    //declare reset function at address 0






// Running loop
void loop() {
  // Check DEMO status
  if (digitalRead(DEMO_PIN) == HIGH) {
    demoMode();
  }
  else {
    // if there's an unsuccessful connection then retry in a bit
    if (!client.connect(server, Port)) {
      die("Connection Failed");
    }
  
      Serial.println("Connected to server");
      Serial.println("Send HTTP request");

      // Make the HTTP request
      client.println(Path);
      client.println(Host);
      client.println("User-Agent: arduino-ethernet");
      client.println("Connection: close");
    if (client.println() == 0) {
      die ("Failed to send request");
    }

    // Check HTTP status
    Serial.println("Check HTTP return code");
    char status[32] = {0};
    client.readBytesUntil('\r', status, sizeof(status));
    if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
      Serial.print(F("Unexpected response: "));
      Serial.println(status);
      die("Did not Get HTTP 200 code");
    }
      
    // Skip HTTP headers
    char endOfHeaders[] = "\r\n\r\n";
        if (!client.find(endOfHeaders)) {
      die ("Invalid response");
    }

    // Now we've got to the body, so we can print it out
    char c;
    String content = "";
    bool foundStart = false;
    bool foundEnd = false;

    // Whilst we haven't timed out & haven't reached the end of the body
    while ( (client.connected() || client.available()) && !foundEnd) {
      c = client.read();
      if (!foundStart && c != '{') {
        continue;
      }
      else if (!foundStart && c == '{') {
        content.concat(c);
        foundStart = true;
      }
      else if (foundStart && c != '}') {
        content.concat(c);
      }
      else if (foundStart && c == '}') {
        content.concat(c);
        foundEnd = true;
      }
    }

    // Serial.println(content);
    processJson(content);

    // Stop the connection to the server.
    Serial.println("Disconnect");
    client.stop();
         
    PrintLocations();
  
      // Move Hands
      for (byte i = 0; i < NUM_PEOPLE; i++) {
        Serial.print("Moving ");
        Serial.print(peopleNames[i]);
        Serial.print(" to position ");
        Serial.println(peopleLocations[i]);
        moveServo(i, peopleLocations[i]);
      }
      
      // Wait 1 minute until next update and blink Yellow LED in 1 sec intervals for 1 min
      Serial.println("Waiting...");
      blinkLed(DATA_LED, 1000, 60);
    }
  }





////////////////////////////////////
// Big Functions To Do Some Stuff //
////////////////////////////////////

// Move a servo
void moveServo (byte& servoNum, const char*& location) {
  int found;
  int counter = 0;
  int tgtLocation;
  
  // Initialize the servo we want to move.
  // We don't leave the servo 'attached' as it draws power and makes excess noise
  peopleServo[servoNum].attach(peopleServoPin[servoNum], servoMin[servoNum], servoMax[servoNum]);
  
  // get the servo's current location (actually, the last value we wrote to it)
  int lastLocation = peopleServo[servoNum].read();
  Serial.print("Servo ");
  Serial.print(servoNum);
  Serial.print(": Last location: ");
  Serial.println(lastLocation);
    
  // find the locations on the clock that match our location
  for (byte i = 0; i < 12; i++) {
    if (strcmp(location, Locations[i]) == 0) {
      found = (i * 10);
      // If the servo is where we left it last, go ahead and rewrite the location value to it and return.
      if (found == lastLocation) {
        Serial.print("Servo ");
        Serial.print(servoNum);
        Serial.println(": No change");
        peopleServo[servoNum].write(lastLocation);
        blinkLed(DATA_LED, 250, 3);
        peopleServo[servoNum].detach();
        return;
      }
      counter++;
    }
  }
    
  tgtLocation = found;
  
  // Some debugging text  
  Serial.print("Servo ");
  Serial.print(servoNum);
  Serial.print(": Moving to location: ");
  Serial.println(tgtLocation);

  // Actually move the servo
  peopleServo[servoNum].write(correctServo(servoNum, tgtLocation));
  blinkLed(DATA_LED, 250, 3);
  peopleServo[servoNum].detach();
  return;
}  


// Process each char returned from the website call
// This will process the JSON that it got back and set the people's locations
void processJson(const String &json) {
  Serial.println("Parse JSON object");
  // Serial.println(json);
  // Allocate the JSON document
  // Use arduinojson.org/v6/assistant to compute the capacity.
  const size_t capacity = JSON_OBJECT_SIZE(5) + 90;
  DynamicJsonDocument doc(capacity);

  // Parse JSON object
  DeserializationError error = deserializeJson(doc, json);
  if (error) {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
    die("Unable to continue");
  }

   Serial.println("Extract values...");
   peopleLocations[0] = doc["Dad"].as<char*>();
   peopleLocations[1] = doc["Mom"].as<char*>();
   peopleLocations[2] = doc["Child1"].as<char*>();
   peopleLocations[3] = doc["Child2"].as<char*>();
   peopleLocations[4] = doc["Child3"].as<char*>();
}





/////////////////////
// Other Functions //
/////////////////////

void demoMode () {
  // Move Hands to random location
  Serial.println("Demo Mode.  Moving hands to random location.");
  for (byte i = 0; i < NUM_PEOPLE; i++) {
    moveServo(i, Locations[random(12)]);
  }

  // Wait 1 minute until next update and blink Yellow LED in 1 sec intervals for 10 min
  Serial.println("Waiting...");
  blinkLed(DATA_LED, 10000, 60);
}


void setupLed () {
  // Go through the LEDs and turn them all on
  pinMode(WIFI_LED, OUTPUT);
  digitalWrite(WIFI_LED, HIGH);
  pinMode(DATA_LED, OUTPUT);
  digitalWrite(DATA_LED, HIGH);
  pinMode(ERR_LED, OUTPUT);
  digitalWrite(ERR_LED, HIGH);
  pinMode(DEMO_PIN, INPUT);
  
  // delay a little bit
  delay(750);
  
  // Turn them all off.
  digitalWrite(WIFI_LED, LOW);  
  digitalWrite(DATA_LED, LOW);
  digitalWrite(ERR_LED, LOW);
 
  return;
}



// Blink an LED on and off for the specified time interval up to the totalTime that we want it blinked.
// for example, a time of 500 and totalTime of 5 would blink the LED on and off every .5 seconds for 5 seconds
// I used this to give some meaning to delay events.
void blinkLed (byte ledPin, unsigned long time, long totalTime) {
  // Convert the totalTime to milliseconds
  long count = long(totalTime * 1000);
  
  // Blink until the timer runs out
  while (count > 0) {
    digitalWrite(ledPin, !digitalRead(ledPin));
    delay(time);
    count = count - time;
  }
  
  return;  
}



// Print out the location array
void PrintLocations () {
  Serial.println("Data from server:");
  for (int i = 0; i < NUM_PEOPLE; i++) {
    Serial.print(peopleNames[i]);
    Serial.print(": ");
    Serial.println(peopleLocations[i]);
  }
}



// Function to correct the servo data to more accurately hit the clock targets.
int correctServo (byte& servoNum, int& input) {
  // read previous value
  int prev = peopleServo[servoNum].read();
  // write a slightly larger or smaller value to the servo
  if (input > prev) {
    peopleServo[servoNum].write(input + servoOffset[servoNum]);
  }
  else {
    peopleServo[servoNum].write(input - servoOffset[servoNum]);
  }
  blinkLed(DATA_LED, 250, 2);
  return input;
}



void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.println("WiFi Data:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  // print the received signal strength:
  Serial.print("signal strength (RSSI): ");
  Serial.print(WiFi.RSSI());
  Serial.println(" dBm");
}


bool die(const char* message) {
  Serial.println(message);
  // while (true);  // loop forever
  // Blink the red LED in .5 sec intervals for 1 min and reset and try again.
  client.stop();
  blinkLed(ERR_LED, 500, 60);
  resetFunc();
}



// The End  :-)
