#!/usr/bin/env python3

import logging
import pymysql
import collections
import json
import yaml
import time
from time import gmtime, strftime


logging.basicConfig(level=logging.ERROR,
    format='(%(threadName)-10s) %(message)s',
)

# Load credentials file
try:
    with open(".credentials.yml", 'r') as ymlfile:
        creds = yaml.load(ymlfile, Loader=yaml.SafeLoader)
        logging.debug(yaml.dump(creds))
except IOError as e:
    logging.critical("Could not read credential file due to I/O error")
    logging.critical(e)
    quit(10)
except:
    logging.critical("Unexpected error reading config file:")
    logging.critical(sys.exc_info()[0])
    quit(10)


# Some global vars
# Minimum accuracy value.  If the phone is really accurate it can throw off the
# reading since the location is just a point value.
# DEFAULT is 50 meters
min_accuracy = 50

# Timeout value (in sec) to trigger mortal peril
# If the last phone data is older than this, then the user is in mortal peril.
# DEFAULT is 2 hours
mortalperil_timeout = 7200
#
#   Calculation factors for converting accuracy of GPS coordinates from meters to degrees of
#   lat/long.  The latitude value is consistent, but the longitude value changes depending on the
#   degree of latitude you are at.  Austin, TX is pretty close to 30.4 deg. latitude, and this is
#   what the calculation is based on.  These numbers were calculated from
#   http://www.csgnetwork.com/degreelenllavcalc.html
#
deg_lat_to_meter = 110859.193020
deg_long_to_meter = 96096.951354
#
db = None
flags = None
Locations = None

# Timestamp and datatime data for right now
current_timestamp = int(time.time())
current_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp))
expires_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp - 2592000))  # 30 day expiry
logging.debug("CURRENT TIMESTAMPS:")
logging.debug("Timestamp: " + str(current_timestamp))
logging.debug("Date Time: " + current_datetime)
logging.debug("Expires Date Time: " + expires_datetime)





################################################################################
#
#   FUNCTIONS
#
################################################################################


def mysql_connect():
    global db
    db = pymysql.connect(host = creds['db_hostname'],
                         user = creds['db_username'],
                         passwd = creds['db_password'],
                         db = creds['db_database'])
    cur = db.cursor(pymysql.cursors.DictCursor)
    return cur
# END mysql_connect


def mysql_disconnect():
    global db
    db.close()
# END mysql_disconnect


# Function to get the data from the user table
def _get_users ():
    global cursor
    query = "SELECT `id`, `username`, `location_source`, `tid` FROM `user`"
    rows = cursor.execute(query)

    result = cursor.fetchall()
    logging.debug("USER LIST:")
    logging.debug(result)
    return result
# END _get_users


# Function to get the latest location entry from the DB for a specific user
def _get_last_db_entry (uid):
    global cursor
    retval = "lost"

    query = "SELECT `u`.`id` AS uid, `h`.`tst` AS timestamp, `h`.`lat` AS latitude, `h`.`lon` AS longitude, `h`.`acc` AS accuracy, `h`.`vel` AS velocity  \
FROM `history` `h`      \
INNER JOIN `user` `u` ON `u`.`tid` = `h`.`tid`    \
WHERE `h`.`tid` = %s ORDER BY timestamp DESC LIMIT 1"

    rows = cursor.execute(query, uid)

    # If no phone data is found -- mortal peril
    if (rows == 0):
        logging.warning("NO DB RECORD FOUND -- MORTAL-PERIL")
        retval = "mortalperil"
    else:
        result = cursor.fetchone()
        logging.debug(result)

        # If the data is old, we also assume mortal peril
        if (int(result['timestamp']) < current_timestamp - mortalperil_timeout):
            logging.warning("Timeout reached: MORTAL PERIL!")
            retval = "mortalperil"
        else:
            known_location = _get_db_known_locations(result)
            # If we search the DB for known locations and come up empty, we see if the user has moved recently to decide whether they are "traveling" or "lost"
            if (known_location == "unknown"):
                # If there is no reported velocity (in km/hr, we will say they are lost.  If there's movement, then they're traveling)
                if (result['velocity'] == None ):
                    retval = "lost"
                else:
                    retval = "traveling"
            # If we found the location in the DB, we'll use that
            else:
                retval = known_location
    

    # return the clock location only
    return retval
# END _get_last_db_entry




# Function to get the location data from the DB based on user, lat, long, and accuracy
def _get_db_known_locations(phone_data):
    global deg_lat_to_meter
    global deg_long_to_meter
    global min_accuracy
    global cursor

    # if the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = int(phone_data['accuracy'])
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = float(phone_data['latitude'])  - lat_acc
    lat_max  = float(phone_data['latitude'])  + lat_acc
    long_min = float(phone_data['longitude']) - long_acc
    long_max = float(phone_data['longitude']) + long_acc

    # Search for locations in the DB that fall within the bounding box
    query = "SELECT `cl`.`name` AS `location`, `l`.`name` \
FROM `location` `l` \
INNER JOIN `clock_location` `cl` \
    ON `cl`.`id` = `l`.`clock_location_id` \
WHERE (`l`.`latitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`longitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`user_id` = \'%s\') \
LIMIT 1"

    result = cursor.execute(query, (lat_min, lat_max, long_min, long_max, phone_data['uid']))

    if result > 0:
        results = cursor.fetchone()
        logging.debug("KNOWN LOCATION FOUND:")
        logging.debug(results)
        return results['location']
    else:
        return "unknown"
# END _get_db_known_locations







# Function to get the latest location entry from the DB for a specific user
# def _get_clock_location (loc_id):
#     global cursor

#     query = "SELECT name AS location \
# FROM `clock_location` \
# WHERE `id` = %s"

#     rows = cursor.execute(query, (loc_id))

#     return retval
# END _get_clock_location


# Function to get the latest location entry from the DB for a specific user
def _get_clock_locations ():
    global cursor
    locationlist = []

    query = "SELECT name AS location FROM `clock_location`"

    rows = cursor.execute(query)
    results = cursor.fetchall()
    for result in results:
        locationlist.append(result['location'])

    logging.debug("LOCATION LIST:")
    logging.debug(locationlist)
    return locationlist
# END _get_clock_locations


# Get the users and their current locations from the DB
def _get_user_locations():
    data = collections.OrderedDict()

    # Get the list of users from the DB
    users = _get_users()

    # Loop through each user
    for user in users:
        if user['location_source'] == 'GPS':
            data[user['username']] = _get_last_db_entry(user['tid'])
        # Add in a bunch here to do Google calendar
        else:
            # fallback to another user's location or a hard-coded location
            # m = re.search('^(clock|user)_(\d+)$', user['location_source'])
            # if m.group(1) == 'user':
            #     data[user['username']] = _get_last_db_entry(m.group(2))
            # elif m.group(1) == 'clock':
            #     data[user['username']] = _get_clock_location(m.group(2))
            # else:
                logging.error("ERROR in specified clock location or user ID")
        # END if
    # END for

    return data
# END _get_user_locations




################################################################################
#
#   MAIN
#
################################################################################

# connect to DB and return cursor
cursor = mysql_connect()

Locations = _get_clock_locations()

# Get the latest user location
data = _get_user_locations()

# Disconnect from the DB
mysql_disconnect()


## OUTPUT
print("Content-type: text/plain\n")
print(json.dumps(data));


