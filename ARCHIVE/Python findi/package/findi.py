# -*- coding: utf-8 -*-
import datetime
import time
import base64
import json
import logging
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
from requests.auth import HTTPBasicAuth
from requests.utils import quote


class FindMyIPhone(object):
    partition = None

    def __init__(self, username, password, device_udid, debug='WARN'):
        if debug == 'DEBUG':
            logging.basicConfig(level=logging.DEBUG,
                format='(%(threadName)-10s) %(message)s',
            )
        elif debug == 'INFO':
            logging.basicConfig(level=logging.INFO,
                format='(%(threadName)-10s) %(message)s',
            )
        else:
            logging.basicConfig(level=logging.WARN,
                format='(%(threadName)-10s) %(message)s',
            )

        self.devices = []
        self.debug = debug
        self.username = username
        self.password = password
        self.udid = device_udid
        self.update_devices()


    def locate(self, device_id, max_wait=300):
        start = int(time.time())
        device = self.devices[device_id]

        while not hasattr(device, 'location_finished') or not device.location_finished:
            logging.info('Waiting for location...')
            if int(time.time()) - start > max_wait:
                raise Exception("Unable to find location within '%s' seconds" % max_wait)
            time.sleep(5)
            self.update_devices()
            device = self.devices[device_id]

        return {
            'latitude': device.latitude,
            'longitude': device.longitude,
            'accuracy': device.horizontal_accuracy,
            'timestamp': device.location_timestamp,
        }


    def send_message(self, device_id, msg, alarm=False, subject='Important Message'):
        device = self.devices[device_id]
        body = json.dumps({
            "clientContext":{
                "appName":"FindMyiPhone",
                "appVersion":"1.4",
                "buildVersion":"145",
                "deviceUDID":self.udid,
                "inactiveTime":5911,
                "osVersion":"3.2",
                "productType":"iPad1,1",
                "selectedDevice":device.id,
                "shouldLocate":False
            },
            "device":device.id,
            "serverContext":{
                "callbackIntervalInMS":3000,
                "clientId":"0000000000000000000000000000000000000000",
                "deviceLoadStatus":"203",
                "hasDevices":True,
                "lastSessionExtensionTime":None,
                "maxDeviceLoadTime":60000,
                "maxLocatingTime":90000,
                "preferredLanguage":"en",
                "prefsUpdateTime":1276872996660,
                "sessionLifespan":900000,
                "timezone":{
                    "currentOffset":-25200000,
                    "previousOffset":-28800000,
                    "previousTransition":1268560799999,
                    "tzCurrentName":"Pacific Daylight Time",
                    "tzName":"America/Los_Angeles"
                },
                "validRegion":True
            },
            "sound":alarm,
            "subject":subject,
            "text":msg,
            "userText":True
        })

        logging.info('Sending message...')
        self.post('/fmipservice/device/%s/sendMessage' % self.username, body)
        logging.info('Message sent')


    def remote_lock(self, device_id, passcode):
        device = self.devices[device_id]
        body = json.dumps({
            "clientContext":{
                "appName":"FindMyiPhone",
                "appVersion":"1.4",
                "buildVersion":"145",
                "deviceUDID":self.udid,
                "inactiveTime":5911,
                "osVersion":"3.2",
                "productType":"iPad1,1",
                "selectedDevice":device.id,
                "shouldLocate":False
            },
            "device":device.id,
            "oldPasscode":"",
            "passcode":passcode,
            "serverContext":{
                "callbackIntervalInMS":3000,
                "clientId":"0000000000000000000000000000000000000000",
                "deviceLoadStatus":"203",
                "hasDevices":True,
                "lastSessionExtensionTime":None,
                "maxDeviceLoadTime":60000,
                "maxLocatingTime":90000,
                "preferredLanguage":"en",
                "prefsUpdateTime":1276872996660,
                "sessionLifespan":900000,
                "timezone":{
                    "currentOffset":-25200000,
                    "previousOffset":-28800000,
                    "previousTransition":1268560799999,
                    "tzCurrentName":"Pacific Daylight Time",
                    "tzName":"America/Los_Angeles"
                },
                "validRegion":True
            }
        })
        logging.info('Sending remote lock...')
        self.post('/fmipservice/device/%s/remoteLock' % self.username, body)
        logging.info('Remote lock sent')


    # No idea if this works
    def remote_wipe(self, device_id):
        device = self.devices[device_id]
        body = json.dumps({
            "clientContext":{
                "appName":"FindMyiPhone",
                "appVersion":"1.4",
                "buildVersion":"145",
                "deviceUDID":self.udid,
                "inactiveTime":5911,
                "osVersion":"3.2",
                "productType":"iPad1,1",
                "selectedDevice":device.id,
                "shouldLocate":False
            },
            "device":device.id,
            "serverContext":{
                "callbackIntervalInMS":3000,
                "clientId":"0000000000000000000000000000000000000000",
                "deviceLoadStatus":"203",
                "hasDevices":True,
                "lastSessionExtensionTime":None,
                "maxDeviceLoadTime":60000,
                "maxLocatingTime":90000,
                "preferredLanguage":"en",
                "prefsUpdateTime":1276872996660,
                "sessionLifespan":900000,
                "timezone":{
                    "currentOffset":-25200000,
                    "previousOffset":-28800000,
                    "previousTransition":1268560799999,
                    "tzCurrentName":"Pacific Daylight Time",
                    "tzName":"America/Los_Angeles"
                },
                "validRegion":True
            }
        })

        logging.warn('Sending remote wipe...')
        self.post('/fmipservice/device/%s/remoteWipe' % self.username, body)
        logging.warn('Remote wipe sent')


    def update_devices(self):
        logging.info('updateDevices...')
        body = json.dumps({
            "clientContext":{
                "appName":"FindMyiPhone",
                "appVersion":"1.4",
                "buildVersion":"145",
                "deviceUDID":self.udid,
                "inactiveTime":2147483647,
                "osVersion":"4.2.1",
                "personID":0,
                "productType":"iPad1,1"
            }
        })
        header, json_obj = self.post('/fmipservice/device/%s/initClient' % self.username, body, None, True)

        if None == json_obj:
            raise Exception('Error parsing json string')

        if json_obj.get('error') is not None:
            raise Exception('Error from web service: %s' % json_obj['error'])

        self.devices = {}
        # log 'Parsing len(json_obj.content) devices...'
        for json_device in json_obj['content']:

            device = Device()
            if json_device.get('location') and type(json_device['location']) is dict:
                device.location_timestamp = datetime.datetime.fromtimestamp(
                    json_device['location']['timeStamp'] / 1000)
                device.location_type = json_device['location']['positionType']
                device.horizontal_accuracy = json_device['location']['horizontalAccuracy']
                device.location_finished = json_device['location']['locationFinished']
                device.longitude = json_device['location']['longitude']
                device.latitude = json_device['location']['latitude']

            device.is_locating = json_device['isLocating']
            device.device_model = json_device['deviceModel']
            device.device_status = json_device['deviceStatus']
            device.id = json_device['id']
            device.name = json_device['name'].encode('utf-8')
            device.deviceClass = json_device['deviceClass']
            device.batteryStatus = json_device['batteryStatus']
            device.batteryLevel = json_device['batteryLevel']

            self.devices[device.id] = device
            logging.info('Found device name: ' + json_device['name'] + ' with ID: ' + json_device['id'])


    def post(self, apiurl, body, headers=None, return_headers=False):
        try:
            if self.partition:
                url = 'https://' + self.partition + apiurl
            else:
                url = 'https://fmipmobile.icloud.com' + apiurl

            logging.debug("URL: " + url)
            logging.debug("POST DATA: " + body)

            headers = headers or {}
            headers.update({
                'Content-type': 'application/json; charset=utf-8',
                'X-Apple-Find-Api-Ver': '2.0',
                'X-Apple-Authscheme': 'UserIdGuest',
                'X-Apple-Realm-Support': '1.0',
                'User-agent': 'Find iPhone/1.2 MeKit (iPad: iPhone OS/4.2.1)',
                'X-Client-Name': 'iPad',
                'X-Client-UUID': '0cf3dc501ff812adb0b202baed4f37274b210853',
                'Accept-Language': 'en-us',
                'Connection': 'keep-alive',
                'Authorization': 'Basic %s' % base64.encodestring('%s:%s' % (self.username, self.password,))[:-1],
            })

            resp = requests.post(url,
                                headers = headers,
                                data    = body,
                                verify  = False)
            if resp.status_code != 200:
                # This means something went wrong.
                resp.raise_for_status()

            # Response code was good.  Get the JSON data
            response = resp.json()
            logging.debug(response)

        # Catch all kinds of exceptions
        except (requests.exceptions.RequestException, ValueError) as err:
            logging.error("ERROR: iCloud API:: "+str(err))
            return False

        # API SUCCESS!
        else:
            # Process the data
            if return_headers:
                return resp.headers, response
            return response



class Device(object):

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return '<Device %s>' % self.name
