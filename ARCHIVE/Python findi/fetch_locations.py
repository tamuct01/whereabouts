#!/usr/bin/env python
from findi import FindMyIPhone
import logging
import time
from time import gmtime, strftime, mktime
import datetime


import yaml
import base64
import pymysql
import re
import collections

logging.basicConfig(level=logging.INFO,
    format='(%(threadName)-10s) %(message)s',
)

# Description:
# This script is used to query iPhone location data and populate the
# Whereabouts clock database.
#
# Global Definitions
# Don't change the rest, please!!
#
# Minimum accuracy value.  If the phone is really accurate it can throw off the
# reading since the location is just a point value.
min_accuracy = 50

# Timeout value (in sec) to trigger mortal peril
# If the last phone data is older than this, then the user is in mortal peril.
mortalperil_timeout = 7200
#
#	Calculation factors for converting accuracy of GPS coordinates from meters to degrees of
#	lat/long.  The latitude value is consistent, but the longitude value changes depending on the
#	degree of latitude you are at.	Austin, TX is pretty close to 30.4 deg. latitude, and this is
#	what the calculation is based on.  These numbers were calculated from
#	http://www.csgnetwork.com/degreelenllavcalc.html
#
deg_lat_to_meter = 110859.193020
deg_long_to_meter = 96096.951354

db = None
cursor = None

# Timestamp and datatime data for right now
current_timestamp = int(time.time())
current_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp))
expires_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp - 2592000))  # 30 day expiry
print "CURRENT TIMESTAMPS:"
print "Timestamp: " + str(current_timestamp)
print "Date Time: " + current_datetime
print "Expires Date Time: " + expires_datetime


# Load credentials file
try:
    with open("/usr/local/apache2/htdocs/.credentials.yaml", 'r') as ymlfile:
        creds = yaml.load(ymlfile, Loader=yaml.SafeLoader)
    # print yaml.dump(cfg)
except IOError as e:
   logging.critical("Could not read credential file due to I/O error({0}): {1}".format(e.errno, e.strerror))
   quit(10)
except:
   logging.critical("Unexpected error reading config file:", sys.exc_info()[0])
   quit(10)


################################################################################
#
#   FUNCTIONS
#
################################################################################


def mysql_connect():
    global db
    db = pymysql.connect(host = creds['db_hostname'],
                         user = creds['db_username'],
                         passwd = creds['db_password'],
                         db = creds['db_database'])
    cur = db.cursor(pymysql.cursors.DictCursor)
    return cur
# END mysql_connect


def mysql_disconnect():
    global db
    db.close()
# END mysql_disconnect


# Function to get the data from the user table
def _get_users():
    global cursor
    query = "SELECT * FROM user"

    result = cursor.execute(query)
    return cursor.fetchall()
# END _get_users


# Function to get the latest location entry from the DB for a specific user
def _get_last_db_entry(uid):
    global cursor

    query = "SELECT h.timestamp, unix_timestamp(h.timestamp) AS unixtime, h.latitude, h.longitude, h.accuracy, cl.name AS location \
FROM `history` `h` \
INNER JOIN `clock_location` `cl` \
	ON `h`.`clock_location_id` = `cl`.`id` \
WHERE `h`.`user_id` = %s \
ORDER BY timestamp DESC \
LIMIT 1"

    result = cursor.execute(query, (uid))
    return cursor.fetchone()
# END _get_last_db_entry


# Function to get the location data from the DB based on user, lat, long, and accuracy
def _get_db_known_locations(uid, phone_data):
    global deg_lat_to_meter
    global deg_long_to_meter
    global min_accuracy
    global cursor

    # if the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = phone_data['accuracy']
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = phone_data['latitude']  - lat_acc
    lat_max  = phone_data['latitude']  + lat_acc
    long_min = phone_data['longitude'] - long_acc
    long_max = phone_data['longitude'] + long_acc

    # Search for locations in the DB that fall within the bounding box
    query = "SELECT `cl`.`name` AS `location`, `l`.`name` \
FROM `location` `l` \
INNER JOIN `clock_location` `cl` \
    ON `cl`.`id` = `l`.`clock_location_id` \
WHERE (`l`.`latitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`longitude` BETWEEN \'%s\' AND \'%s\' AND \
    `l`.`user_id` = \'%s\') \
LIMIT 1"

    result = cursor.execute(query, (lat_min, lat_max, long_min, long_max, uid))

    if result > 0:
        results = cursor.fetchone()
        return results['location']
    else:
        return "unknown"
# END _get_db_known_locations


# Function to get the clock_location ID based on the name
def _get_clock_location(location):
    global cursor

    query = "SELECT `id` FROM `clock_location` WHERE `name` = %s LIMIT 1"
    result = cursor.execute(query, (location))

    if result == 1:
        results = cursor.fetchone()
        return results['id']
    else:
        return None
# END _get_clock_location


# Update the DB with the latest phone data from a user
def _update_db(uid, phone_data):
    global cursor
    global db

    # Get the clock location ID based on name
    clock_id = _get_clock_location(phone_data['location'])

    # Insert the new data into the DB
    query = "INSERT INTO `history` (`user_id`, `timestamp`, `latitude`, `longitude`, `accuracy`, `clock_location_id`) \
VALUES (%s, %s, %s, %s, %s, %s)"

    cursor.execute(query, (uid, phone_data['timestamp'], phone_data['latitude'], phone_data['longitude'], phone_data['accuracy'], clock_id))
    db.commit()
# END _update_db


# Function to check to see if the new phone data is near the most recent location
def _spot_check(last_data, phone_data):
    global deg_lat_to_meter
    global deg_long_to_meter
    global min_accuracy

    # If the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = phone_data['accuracy']
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = phone_data['latitude']  - lat_acc
    lat_max  = phone_data['latitude']  + lat_acc
    long_min = phone_data['longitude'] - long_acc
    long_max = phone_data['longitude'] + long_acc

    # The latest phone data was near the last data (i.e. the user is in the same place)
    if  last_data['latitude']  >= lat_min  and \
        last_data['latitude']  <= lat_max  and \
        last_data['longitude'] >= long_min and \
        last_data['longitude'] <= long_max:
            return True
    # The user has moved
    else:
        return False
# END _spot_check


# Remove old data from the history table so it doesn't get massive
def _expire_old_history():
    global db
    global cursor
    global expires_datetime

    query = "DELETE FROM `history` WHERE `timestamp` < %s"

    cursor.execute(query, (expires_datetime))
    db.commit()
    
# END _expire_old_history



################################################################################
#
#   MAIN
#
################################################################################

# connect to DB and return cursor
cursor = mysql_connect()

# Get the user data from the database
users = _get_users()

# Loop through each user
for user in users:
    #
    # Look at the location_source field and determine what to do
    # For GPS, get the GPS data from the phone
    # For clock_<ID>, do nothing.  The clock code will grab that.
    # For user_<ID>, do nothing, The clock code will handle that as well.
    #
    if (user['location_source'] != 'GPS'):
        continue

    # logging.debug("User data from DB table: " + str(user))

    # Populate the previous data to compare against.
    last_data             = _get_last_db_entry(user['id'])
    logging.info("Last DB location data from DB: " + str(last_data))

    # Get the phone's location from the iCloud site
	# Enter your iCloud username and password
    ssm = FindMyIPhone(user['icloud_username'], user['icloud_password'], user['device_udid'])
    try:
        phone_data             = ssm.locate(user['device_id'])
        phone_data['unixtime'] = mktime(phone_data['timestamp'].timetuple())
    except Exception as e:
        print "\nException in locating user: {}".format(e)
        phone_data['unixtime'] = last_data['unixtime']

    # Print a comparision of last and current data
    print "\n"
    print "{0:11} {1:26} | {2:10} {3:26}".format("LAST DATA:", user['username'], "PHONE DATA:", user['username'])
    print "--------------------------------------------------------------------------------"
    print "{0:11} {1:26} | {2:10} {3:26}".format("Timestamp:", last_data['unixtime'],  "Timestamp:", phone_data['unixtime'])
    print "{0:11} {1:>26} | {2:10} {3:>26}".format("DateTime:",  str(last_data['timestamp']), "DateTime:",  str(phone_data['timestamp']))
    print "{0:11} {1:26} | {2:10} {3:26}".format("Latitude:",  last_data['latitude'],  "Latitude:",  phone_data['latitude'])
    print "{0:11} {1:26} | {2:10} {3:26}".format("Longitude:", last_data['longitude'], "Longitude:", phone_data['longitude'])
    print "{0:11} {1:26} | {2:10} {3:26}".format("Accuracy:",  last_data['accuracy'],  "Accuracy:",  phone_data['accuracy'])
    print "{0:11} {1:26} | ".format("Location:",  last_data['location']),

    # If the date in the phone is newer than our last known location, then we have a new
    # entry and will do something with it
    if phone_data['unixtime'] > last_data['unixtime']:
        # we have a new data -- check to see where we're at.
        phone_data['location'] = _get_db_known_locations(user['id'], phone_data)

        print "{0:11} {1:26}".format("Location:", phone_data['location'])
        print "--------------------------------------------------------------------------------"
        print "{0:11} {1:26}".format("", "NEW PHONE DATA!")

        # Check to see if we're in the same location, but just have an updated entry
        if phone_data['location'] == last_data['location']:
            # same place, just update the log with the new timestamp
            _update_db(user['id'], phone_data)
            print "{0:>38}\n".format("No change.  Still at: " + phone_data['location'])

        # We've moved!  do some checking to see where we're at
        else :
            # If the new location is unknown we'll say we're traveling.
            if phone_data['location'] == "unknown":
                phone_data['location'] = "traveling"

            # check to see if phone is in an unknown location, but in same spot.
            # If we're in an unknown location (traveling), but still in the same spot we're "lost"
            if _spot_check(last_data, phone_data) and phone_data['location'] == "traveling" and \
                (last_data['location'] == "traveling" or last_data['location'] == "lost"):
                    phone_data['location'] = "lost"

            # Update the DB with the location
            _update_db(user['id'], phone_data)
            print "{0:>38}\n".format("Moved!  New loc: " + phone_data['location'])
        # END if

    # No new phone data -- NOTE:  Need to check if this logic works.
    else:
        print "\n{0:11} {1:26} | ".format("", "NO NEW DATA!")
        # If the timestamp is older than our mortalperil timeout, update us to mortalperil.
        if phone_data['unixtime'] < current_timestamp - mortalperil_timeout:
            phone_data['location']  = 'mortalperil'
            phone_data['timestamp'] = current_datetime
            phone_data['latitude']  = 00.0000
            phone_data['longitude'] = 00.0000
            phone_data['accuracy']  = 0
            _update_db(user['id'], phone_data)
            print "{0:38}\n".format("Timeout reached: MORTAL PERIL!")
        else:
            print "{0:38}\n".format("No new DB entry")
    # END if

# END user loop

# Delete old entries
_expire_old_history()

# Disconnect from the DB
mysql_disconnect()
