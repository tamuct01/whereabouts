#!/usr/bin/env python
# from __future__ import print_function
import httplib2
import os
import sys

import base64
import pymysql
import re
import collections
import json
import yaml
import datetime
from datetime import timedelta

from googleapiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

print "Content-type: text/plain\n"

# Load credentials file
try:
    with open(".credentials.yaml", 'r') as ymlfile:
        creds = yaml.load(ymlfile, Loader=yaml.SafeLoader)
        # print yaml.dump(creds)
except IOError as e:
    print "Could not read credential file due to I/O error({0}): {1}".format(e.errno, e.strerror)
    quit(10)
except:
    print "Unexpected error reading config file:", sys.exc_info()[0]
    quit(10)


# Some global vars
db = None
flags = None
Locations = None
# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
CLIENT_SECRET_FILE = '.client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'



################################################################################
#
#   FUNCTIONS
#
################################################################################


def mysql_connect():
    global db
    db = pymysql.connect(host = creds['db_hostname'],
                         user = creds['db_username'],
                         passwd = creds['db_password'],
                         db = creds['db_database'])
    cur = db.cursor(pymysql.cursors.DictCursor)
    return cur
# END mysql_connect


def mysql_disconnect():
    global db
    db.close()
# END mysql_disconnect


# Function to get the data from the user table
def _get_users ():
    global cursor
    query = "SELECT `id`, `username`, `location_source` FROM `user`"
    rows = cursor.execute(query)

    return cursor.fetchall()
# END _get_users


# Function to get the latest location entry from the DB for a specific user
def _get_last_db_entry (uid):
    global cursor

    query = "SELECT h.timestamp, cl.name AS location \
FROM `history` `h` \
INNER JOIN `clock_location` `cl` \
	ON `h`.`clock_location_id` = `cl`.`id` \
WHERE `h`.`user_id` = %s \
ORDER BY h.timestamp DESC \
LIMIT 1"

    rows = cursor.execute(query, (uid))
    result = cursor.fetchone()

    # return the clock location only
    return result['location']
# END _get_last_db_entry


# Function to get the latest location entry from the DB for a specific user
def _get_clock_location (loc_id):
    global cursor

    query = "SELECT name AS location \
FROM `clock_location` \
WHERE `id` = %s"

    rows = cursor.execute(query, (loc_id))
    result = cursor.fetchone()

    return result['location']
# END _get_clock_location


# Function to get the latest location entry from the DB for a specific user
def _get_clock_locations ():
    global cursor
    locationlist = []

    query = "SELECT name AS location FROM `clock_location`"

    rows = cursor.execute(query)
    results = cursor.fetchall()
    for result in results:
        locationlist.append(result['location'])

    return locationlist
# END _get_clock_locations


def _get_google_calendar(username):
    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)
    calendar = creds['calendar'][username]


    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    now15 = (datetime.datetime.utcnow() + timedelta(minutes=5)).isoformat() + 'Z'
    #print('Getting current event')
    eventsResult = service.events().list(
        calendarId=calendar, timeMin=now, timeMax=now15, maxResults=1,
        singleEvents=True, orderBy='startTime').execute()
    events = eventsResult.get('items')

    if not events:
        #print('No upcoming events found.')
        return None
    # for event in events:
    else:
        event = events[0]
        for location in Locations:
            if re.search(location, event['summary'], re.IGNORECASE):
                return location

    return None
# END _get_google_calendar




def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
#    home_dir = os.path.expanduser('~')
 #   credential_dir = os.path.join(home_dir, '.credentials')
    credential_dir = os.path.join('.credentials')

    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials
# END get_credentials





# Get the users and their current locations from the DB
def _get_user_locations():
    data = collections.OrderedDict()

    # Get the list of users from the DB
    users = _get_users()

    # Loop through each user
    for user in users:
        if user['location_source'] == 'GPS':
            data[user['username']] = _get_last_db_entry(user['id'])
        # Add in a bunch here to do Google calendar
        else:
            # Do google calendar first before defaulting to a clock location or another user's locations
            google_location = _get_google_calendar(user['username'])
            if google_location:
                data[user['username']] = google_location
            else:
                # fallback to another user's location or a hard-coded location
                m = re.search('^(clock|user)_(\d+)$', user['location_source'])
                if m.group(1) == 'user':
                    data[user['username']] = _get_last_db_entry(m.group(2))
                elif m.group(1) == 'clock':
                    data[user['username']] = _get_clock_location(m.group(2))
                else:
                    print "ERROR in specified clock location or user ID"
            # END if
        # END if
    # END for

    return data
# END _get_user_locations




################################################################################
#
#   MAIN
#
################################################################################

# connect to DB and return cursor
cursor = mysql_connect()

Locations = _get_clock_locations()

# Get the latest user location
data = _get_user_locations()

# Disconnect from the DB
mysql_disconnect()

print json.dumps(data);

