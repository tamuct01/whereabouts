# This code was never used in a production relase, but was an interesting test.


import os
import logging
import boto3
import json
import time
# from time import gmtime, strftime, mktime
from functools import reduce 

### GLOBAL VARS

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# define the DynamoDB table that Lambda will connect to
tableName = "gps_locations"
# create the DynamoDB resource
dynamo = boto3.client("dynamodb", region_name='us-east-2')

# Lambda proxy return object
retval = {
    "statusCode": 404,
    "body": ""
}

# Minimum accuracy value (in meters).  If the phone is really accurate it can throw off the
# reading since the location is just a point value.
min_accuracy = 50

# Timeout value (in sec) to trigger mortal peril
# If the last phone data is older than this, then the user is in mortal peril.
mortalperil_timeout = 7200
#
#	Calculation factors for converting accuracy of GPS coordinates from meters to degrees of
#	lat/long.  The latitude value is consistent, but the longitude value changes depending on the
#	degree of latitude you are at.	Austin, TX is pretty close to 30.4 deg. latitude, and this is
#	what the calculation is based on.  These numbers were calculated from
#	http://www.csgnetwork.com/degreelenllavcalc.html
#
deg_lat_to_meter = 110859.193020
deg_long_to_meter = 96096.951354

# Timestamp and datatime data for right now
current_timestamp = int(time.time())
# current_datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime(current_timestamp))




### FUNCTION DEFINITIONS

# Create a query to get user info
def create_user_query(tableName, userName):
    return {
        "TableName": tableName,
        "KeyConditionExpression": '#PK = :PK and #SK = :SK',
        "Limit": 1,
        "ExpressionAttributeNames": {"#PK":"PK","#SK":"SK"},
        "ExpressionAttributeValues": {":PK": {"S":"USER#"+userName},":SK": {"S":"USER#"+userName}}
    }


# Create a query to get all the locations for the user
def create_user_location_query(tableName, userName):
    return {
        "TableName": tableName,
        "KeyConditionExpression": '#PK = :PK and begins_with(#SK, :SK)',
        "ExpressionAttributeNames": {"#PK":"PK","#SK":"SK"},
        "ExpressionAttributeValues": {":PK": {"S":"USER#"+userName},":SK": {"S":"LOC#"}}
    }

# Create a query to get all the locations for the user
def create_last_location_query(tableName, userName):
    return {
        "TableName": tableName,
        "KeyConditionExpression": '#PK = :PK and #SK = :SK',
        "Limit": 1,
        "ExpressionAttributeNames": {"#PK":"PK","#SK":"SK"},
        "ExpressionAttributeValues": {":PK": {"S":"USER#"+userName},":SK": {"S":"GPS#"+userName}}
    }

# Create a query to update last location field for the user
def create_last_location_update_query(tableName, userName, phoneData):
    return {
        "TableName": tableName,
        "Key": {
            "PK": {"S":"USER#"+userName}, 
            "SK": {"S":"GPS#"+userName}
        },
        "UpdateExpression": "SET #TK = :TK",
        "ExpressionAttributeNames": {"#TK":"other_attributes"},
        "ExpressionAttributeValues": {":TK": {"S":json.dumps(phoneData)}}
    }

# create a query to update the user's record with timestamp and location
def create_user_update_query(tableName, userName, userData):
    return {
        "TableName": tableName,
        "Key": {
            "PK": {"S":"USER#"+userName}, 
            "SK": {"S":"USER#"+userName}
        },
        "UpdateExpression": "SET #TK = :TK",
        "ExpressionAttributeNames": {"#TK":"other_attributes"},
        "ExpressionAttributeValues": {":TK": {"S":json.dumps(userData)}}
    }


# creaate a query to scan for all the user data (Used in GET)
def create_user_scan_query(tableName):
    return {
        "TableName": tableName,
        "FilterExpression": "begins_with(#PK, :PK) And begins_with(#SK, :SK)",
        "ExpressionAttributeNames": {"#PK":"PK","#SK":"SK"},
        "ExpressionAttributeValues": {":PK": {"S":"USER#"},":SK": {"S":"USER#"}}
    }



# execute the DynamoDB query given
def execute_query(dynamodb_client, input):
    try:
        queryResponse = dynamodb_client.query(**input)
        logger.info("DynamoDB Query Response: ")
        logger.info(queryResponse)
        return queryResponse
        # Handle response
    except Exception as error:
        print("Unknown error while querying: ", error)


# execute the DynamoDB update given
def execute_update_item(dynamodb_client, input):
    try:
        queryResponse = dynamodb_client.update_item(**input)
        logger.info("DynamoDB Item Updated Successfully")
        logger.info(queryResponse)
        # Handle response
    except Exception as error:
        print("Unknown error while updating: ", error)


# execute the DynamoDB scan given
def execute_scan_query(dynamodb_client, input):
    try:
        queryResponse = dynamodb_client.scan(**input)
        logger.info("DynamoDB Scan Successful")
        logger.info(queryResponse)
        return queryResponse
        # Handle response
    except Exception as error:
        print("Unknown error while scanning: ", error)




# safely get deep dictionaries
def deep_get(_dict, keys, default=None):

    def _reducer(d, key):
        if isinstance(d, dict):
            return d.get(key, default)
        return default

    return reduce(_reducer, keys, _dict)


# get the user key and user name from the event
def get_user_key_name(event):
    user_key = deep_get(event, ['headers', 'x-user-key'])
    user_name = deep_get(event, ['headers', 'x-user-name'])
        
    # Need proper headers
    if (user_key == None or user_name == None):
        raise Exception("Invalid User headers")
    else:
        return user_key, user_name


# Function to check to see if the new phone data is near the most recent location
def _spot_check(last_data, phone_data):
    # If the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = phone_data['horizontal_accuracy']
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = phone_data['latitude']  - lat_acc
    lat_max  = phone_data['latitude']  + lat_acc
    long_min = phone_data['longitude'] - long_acc
    long_max = phone_data['longitude'] + long_acc

    # The latest phone data was near the last data (i.e. the user is in the same place)
    if  float(last_data['latitude'])  >= lat_min  and \
        float(last_data['latitude'])  <= lat_max  and \
        float(last_data['longitude']) >= long_min and \
        float(last_data['longitude']) <= long_max:
            return True
    # The user has moved
    else:
        return False




# validate user
def validate_user(user_key, user_name):
    # Call DynamoDB's query API for the user
    queryResponse = execute_query(dynamo, create_user_query(tableName, user_name))

    # Should only have 1 record
    if (queryResponse.get('Count') < 1):
        raise Exception("User not found")
    else:
        # Check User UUID
        Items = queryResponse.get('Items')
        UserRecord = Items[0]
        dbUserData = json.loads(str(deep_get(UserRecord, ['other_attributes', 'S'])))
        logger.info(dbUserData)

        if (dbUserData.get('uuid') != user_key):
            raise Exception("Invalid User credentials")
        else:
            logger.info("User "+user_name+" found in the database.  Proceeding...")
            return dbUserData


def get_clock_location(user_name, phone_data):
    retval = 'unknown'
    # if the accuracy reporting from the phone is too small it can generate false reports.
    # Here we'll adjust the accuracy to a good minimum
    acc = phone_data.get('horizontal_accuracy')
    if acc < min_accuracy:
        acc = min_accuracy

    # Convert the accuracy to degrees
    lat_acc  = acc / deg_lat_to_meter
    long_acc = acc / deg_long_to_meter

    # Create a bounding box around the phone location
    lat_min  = phone_data.get('latitude')  - lat_acc
    lat_max  = phone_data.get('latitude')  + lat_acc
    long_min = phone_data.get('longitude') - long_acc
    long_max = phone_data.get('longitude') + long_acc

    # Call DynamoDB's query API for the user
    locationsQuery = execute_query(dynamo, create_user_location_query(tableName, user_name))
    if (locationsQuery.get('Count') < 1):
        raise Exception("No locations found for the user")
    
    locations = locationsQuery.get('Items')
    for location in locations:
        loc = json.loads(str(deep_get(location, ['other_attributes', 'S'])))
        if (float(loc.get('latitude')) > lat_min and float(loc.get('latitude')) < lat_max and float(loc.get('longitude')) > long_min and float(loc.get('longitude')) < long_max):
            retval = loc.get('clock_name')
            logging.info("Found location: "+retval)

    return retval


def check_last_location(user_name, phone_data):
    # Call DynamoDB's query API for the user
    queryResponse = execute_query(dynamo, create_last_location_query(tableName, user_name))
    retval = phone_data['clock_location']

    # Should only have 1 record
    if (queryResponse.get('Count') < 1):
        # No stored location found
        #raise Exception("User not found")
        logger.info("No previous location found")
    else:
        # Found last location
        Items = queryResponse.get('Items')
        LocRecord = Items[0]
        last_data = json.loads(str(deep_get(LocRecord, ['other_attributes', 'S'])))
        # logger.info(last_data)

        # If the new location is unknown we'll say we're traveling.
        if phone_data['clock_location'] == "unknown":
            retval = "traveling"
            logger.info("### User is traveling")

        # check to see if phone is in an unknown location, but in same spot.
        # If we're in an unknown location (traveling), but still in the same spot we're "lost"
        if _spot_check(last_data, phone_data) and retval == "traveling" and \
            (last_data['clock_location'] == "traveling" or last_data['clock_location'] == "lost"):
                retval = "lost"
                logger.info("### User is lost")
    
    logger.info("### User is NOT traveling or lost")
    return retval


# scan the table for user data that contains the latest timestamp and clock location
def get_last_locations():
    # Call DynamoDB's query API for all users
    queryResponse = execute_scan_query(dynamo, create_user_scan_query(tableName))
    retval = dict()
    
    # Should have multiple records
    if (queryResponse.get('Count') < 1):
        # No stored location found
        raise Exception("No User Records Found")
    else:
        # Found user(s) location
        Items = queryResponse.get('Items')
        for record in Items:
            data = json.loads(str(deep_get(record, ['other_attributes', 'S'])))
            dbname = data.get('name')
            logger.info("Found user "+dbname)
            if (dbname == None or data.get('last_timestamp') == None):
                continue
            
            # logger.info("### DB last timestamp: "+ str(data.get('last_timestamp')))
            # logger.info("### Current timestamp: "+ str(current_timestamp))

            if (current_timestamp - mortalperil_timeout > int(data.get('last_timestamp'))):
                retval[dbname] = "mortalperil"
            elif (data.get('last_location') == None or data.get('last_location') == "unknown"):
                retval[dbname] = "lost"
            else:
                retval[dbname] = data.get('last_location')
    
    return retval








### MAIN HANDLER

def lambda_handler(event, context):
    logger.info('## STARTING FUNCTION\r' + json.dumps(event))

    # POST new data from phones
    if (event.get('httpMethod') == 'POST'):
        # Validate the user
        try:
            (user_key, user_name) = get_user_key_name(event)
            db_user_data = validate_user(user_key, user_name)
        except Exception as error:
            logger.error(error.args)
            retval['statusCode'] = 500
            retval['body'] = json.dumps(str(error.args))
            return (retval)
        
        # Take phone data and look for matching locations
        phone_data = json.loads(str(event.get('body')))
        if (phone_data.get('latitude') == None or phone_data.get('longitude') == None):
            retval['statusCode'] = 500
            retval['body'] = "No valid phone data"
            return retval
        
        logger.info("### Checking phone data against known locations in DB")
        phone_data['clock_location'] = get_clock_location(user_name, phone_data)
        logging.info("### FOUND LOCATION: "+phone_data['clock_location'])
        # logger.info(json.dumps(phone_data))

        logger.info("### Checking user for traveling or lost conditions")
        phone_data['clock_location'] = check_last_location(user_name, phone_data)

        # Put the last location data in the user record to minimize queries for the GET function
        db_user_data['last_timestamp'] = phone_data['timestamp']
        db_user_data['last_location'] = phone_data['clock_location']


        # update database record
        try:
            logger.info("### Updating last location record")
            queryResponse = execute_update_item(dynamo, create_last_location_update_query(tableName, user_name, phone_data))
            logger.info("### Updating user record")
            queryResponse = execute_update_item(dynamo, create_user_update_query(tableName, user_name, db_user_data))

        except Exception as error:
            logger.error(error.args)
            retval['statusCode'] = 500
            retval['body'] = json.dumps(str(error.args))
            return (retval)


        # fallthrough and return phone data if successful        
        retval['statusCode'] = 200
        retval['body'] = json.dumps(phone_data)
        # logger.info(json.dumps(phone_data))
    
    # Return last data on everyone
    elif (event.get('httpMethod') == 'GET'):
        # AWS API Gateway will validate APIKEY for minimal security
        last_locations = get_last_locations()

        retval['statusCode'] = 200
        retval['body'] = json.dumps(last_locations)
        




    
    else:
        retval['statusCode'] = 500
        retval['body'] = "Invalid httpMethod"

    # Fallthrough and return the object as modified, or a 404
    return (retval)
    




