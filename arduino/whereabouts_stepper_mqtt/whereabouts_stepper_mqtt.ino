#include <AccelStepper.h>
#include <ezButton.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <RTClib.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <NTPClient_Generic.h>

// Define Constants
// Some constants/vars for LED data and timing
// LED pin assignments for status indicators
#define WIFI_LED A0
#define DATA_LED A1
#define ERR_LED  A2
#define DEMO_PIN A3
#define MSG_BUFFER_SIZE	512
#define MORTAL_PERIL_TIMEOUT 86400 //24 hours in seconds 
#define NTP_POLL_INTERVAL 3600 //1 hour = 3600

// comment or uncomment to enable features (for testing)
#define ENABLE_WIFI
// #define DEBUG
#define ENABLE_MOTORS

#ifdef ENABLE_WIFI
  // WIFI INFO - UPDATE WITH YOUR WIFI AND HOST INFORMATION
  /* Network Information */
  const char* ssid = "NETWORK_SSID";                // your network SSID (name)
  const char* pass = "NETWORK_PASS";                // your network password
  const char* mqtt_server = "192.168.100.10";       // MQTT server address (ex. "192.168.100.10")
  const char* mqtt_username = "MQTT_USER";          // your MQTT username
  const char* mqtt_password = "MQTT_PASS";          // your MQTT password
  const char* ntp_server = "time.cloudflare.com";   // your NTP server (or use this one)

  // WiFi/Ethernet vars
  WiFiUDP ntpUDP;
  NTPClient timeClient(ntpUDP, ntp_server, 0, NTP_POLL_INTERVAL);

  WiFiClient wifiClient;
  PubSubClient mqttClient(wifiClient);
#endif

// PERSONAL INFO - UPDATE WITH YOUR INFORMATION
// The number of people (or hands) that I have on the clock
#define NUM_PEOPLE 5
// Names of the people we track (must match what is returned from the server -- This is the TID from Owntracks that I've set to the person's initials
const char* peopleNames[NUM_PEOPLE] = {"PERSON1", "PERSON2", "PERSON3", "PERSON4", "PERSON5"};
// The locations that we will fill from the server
String peopleLocations[NUM_PEOPLE] = { "", "", "", "", "" };
// The timestamps of the last data from each person
unsigned long peopleTimestamps[NUM_PEOPLE] = {0,0,0,0,0};
// The locations we know about that reference positions around the clock starting with mortal peril at the 12 o'clock position
String Locations[] = { "mortalperil", \
                            "holidays",    \
                            "hospital",    \
                            "home",        \
                            "quidditch",   \
                            "school",      \
                            "work",        \
                            "prison",      \
                            "traveling",   \
                            "church",      \
                            "lost",        \
                            "play"
                          };

// Define stepper motor pins, pointer array, and parameters
#define MAX_MOTOR_RUN 2     // Number of motors to run simultaneously.  This is limited by the amount of current we can draw from the power supply.
unsigned char stepperPins[NUM_PEOPLE][4] = {
  {22, 23, 24, 25},
  {26, 27, 28, 29},
  {30, 31, 32, 33},
  {34, 35, 36, 37},
  {38, 39, 40, 41},  
};
float MaxSpeed = 500.0;   // any faster than this and the motor skips steps
float Accel = 500.0;

#define HALFSTEP 8    //Half step full rotation is 4096 steps (3072 after clock gearing)
#define FULLSTEP 4    //Full step full rotation is 2048 steps (1536 after clock gearing)
#define MAX_POSITION 8000   // equivalent to about 5 turns.  if this is reached, there's something wrong
#define FULL_TURN 1536    // One full turn of steps
#define REVERSE -1   // My clock was running counter-clockwise for positive values, so this is used to send inverted numbers to the steppers.  Set to 1 to use the normally calculated values. 
AccelStepper *stepperPtrArray[NUM_PEOPLE];

// Stepper initialization variables
long stepperLeftPos[NUM_PEOPLE] = {0, 0, 0, 0, 0};
long stepperRightPos[NUM_PEOPLE] = {0, 0, 0, 0, 0};
byte stepperTouchCount[NUM_PEOPLE] = {0, 0, 0, 0, 0};
//

// limit switch declarations
ezButton *switchPtrArray[NUM_PEOPLE];
unsigned char switchPins[NUM_PEOPLE] = {A8, A9, A10, A11, A12};
//

// Real-Time Clock (RTC) definitions and other timers
RTC_DS3231 rtc;
unsigned long lastClockSync = 0;
unsigned long lastDisplay = millis();
bool msgReceived = false;
//

// Function prototypes
void(* resetFunc) (void) = 0;    //declare reset function at address 0
bool die(const char* message, bool retry=true);
int motorStopped(int motorIndex, bool motorInitialize = false);
bool moveSteppers(long * destinations, int size, bool motorInitialize = false);
//



// Setup
void setup() {
  Serial.begin(115200);
  Serial.println(F("\n\nInitializing Hardware...\n"));
  randomSeed(analogRead(A15));

  // Initialize the LEDs.
  setupLed();

  // Initialize DS3231
  Serial.println("Initialize DS3231 RTC...");;
  rtc.begin();
  printRtcTime();


  #ifdef ENABLE_WIFI
    // *----
    // WIFI Initilization
    // *----
    int wifi_status = WL_IDLE_STATUS;     // the Wifi radio's status  
    // Print status to serial
    Serial.println(F("Starting WiFi Ethernet and waiting to stabilize..."));

    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
      Serial.println(F("WiFi shield not present"));
      die("Error in Wifi", false);
    }

    // Wait for a connection
    while (wifi_status != WL_CONNECTED) {
      // Attempt to connect to the Wifi network:
      Serial.print(F("Attempting to connect to WPA SSID: "));
      Serial.println(ssid);
      // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
      wifi_status = WiFi.begin(ssid, pass);

      // wait 10 seconds for connection
      blinkLed(WIFI_LED, 1000, 10);
    }

    // Connected!
    Serial.println(F("Connected to WiFi"));
    digitalWrite(WIFI_LED, HIGH);
    printWifiStatus();

    // Start the NTP client and update the RTC module with the NTP date/time
    timeClient.begin();  // Start NTP client
    updateRtcFromNtp();

    // Set client timeout
    // client.setTimeout(10000);
    delay(1000);
    mqttClient.setServer(mqtt_server, 1883);
    mqttClient.setBufferSize(MSG_BUFFER_SIZE);
    mqttClient.setCallback(callback);
  #endif

  // *----

  #ifdef ENABLE_MOTORS
    // *----
    // Stepper Motor Initilization and Homing routing
    // *----
    // Initialze the stepper motor variables, etc.
    initializeSteppers();

    // Home the steppers using the microswitches
    long initialDestinations[NUM_PEOPLE] = {};
    if (moveSteppers(initialDestinations, NUM_PEOPLE, true) == false) {
      Serial.println(F("Error initializing stepper motors.  Could not home motors."));
      die("Error in initializing motors", false);
    }
    else {
      Serial.println(F("Motors initialized\n"));
    }
    // *----
  #endif
}



// *----
// Running loop
// *----
void loop() {
  // Add a description here about DEMO MODE
  // Check DEMO status
  if (digitalRead(DEMO_PIN) == HIGH) {
    demoMode();
    #ifdef ENABLE_MOTORS
      // Move motors appropriately
      moveMotors();
    #endif
    blinkLed(DATA_LED, 5000, 60); // set wait time to 5 mins so the clock isn't always moving
  }

  // NOT DEMO Mode
  else {
    // Get current location from the server
    #ifdef ENABLE_WIFI
      // getCurrentLocation();

      if (!mqttClient.connected()) {
        reconnect();
      }
    #endif
 
    #ifdef ENABLE_MOTORS
      // Move motors appropriately
      if (msgReceived) {
        moveMotors();
        msgReceived = false;
      }
    #endif

    #ifdef ENABLE_WIFI
      // Wait for message
      mqttClient.loop();

      // Check for NTP server sync time
      if ((rtc.now().unixtime() - lastClockSync) > NTP_POLL_INTERVAL) {
        Serial.println(F("Updating NTP..."));
        updateRtcFromNtp();
      }
    #endif
  }

  // Print an update every 5 mins
  if (millis() - lastDisplay > 300000) {
    lastDisplay = millis();
    printRtcTime();
    PrintLocations();
  }
}


// *----
// Function that is called when a MQTT message is received.
// This parses the JSON message and updates the arrays with the user's location and timestamp.
// *----
void callback(char* topic, byte* payload, unsigned int length) {
  JsonDocument doc;

  // print raw JSON
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
    
  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  // Fetch the values
  //
  // Most of the time, you can rely on the implicit casts.
  // In other case, you can do doc["time"].as<long>();
  const char* personid = doc["tid"];  // person ID
  unsigned long time = doc["tst"];             // timestamp
  const char* msgtype = doc["_type"]; // type of message

  // LWT (last will and testament) messages don't have the tid value set.  Let's shortcut that checking here.
  if (strcmp(msgtype, "lwt") == 0) {              // last will and testament
    return;
  }
  else {
    msgReceived = true;
  }

  // Get our person ID
  int personArrIndex = arrayFind(personid, peopleNames, NUM_PEOPLE);
  // check that the "tid" person exists in our person Array
  if (personArrIndex < 0 ) {
    Serial.print(F("person with ID: "));
    Serial.print(personid);
    Serial.println(F(" not found in peopleNames array."));
    return;
  }

  // add the timestamp into the user array
  peopleTimestamps[personArrIndex] = time;

  // select based on message type.  According to OwnTracks documents, this can be a number of things, only a few we care about
  // https://owntracks.org/booklet/tech/json/

  // Check to see how old the message is.  If it's too old we assume Mortal Peril
  if ((rtc.now().unixtime() - time) > MORTAL_PERIL_TIMEOUT) {
    Serial.print(F("User "));
    Serial.print(personid);
    Serial.println(F(" is in Mortal Peril"));
    peopleLocations[personArrIndex] = "mortalperil";
    PrintLocations();
    return;
  }

  // LOCATION MESSAGE
  if (strcmp(msgtype, "location") == 0) {
    // const char* inregions = doc["inregions"][0];
    const char* inrids = doc["inrids"][0];
    if (inrids == NULL) {   // No region detected -- could be traveling or lost
      Serial.print(F("User "));
      Serial.print(personid);
      Serial.println(F(" is traveling or lost"));
      // strcpy(peopleLocations[personArrIndex], "traveling");
      peopleLocations[personArrIndex] = "traveling";
      // something else for LOST?

    }
    else {  // currently in a known region
      char* shortloc = strtok(inrids, "-");
      Serial.print(F("User "));
      Serial.print(personid);
      Serial.print(F(" is at "));
      Serial.println(shortloc);
      // strcpy(peopleLocations[personArrIndex], shortloc);
      peopleLocations[personArrIndex] = shortloc;
    }
  }
  // TRANSITION TO/FROM REGION MESSAGE
  else if (strcmp(msgtype, "transition") == 0) {
    const char* event = doc["event"];   // "enter" or "leave"
    // const char* desc  = doc["desc"];
    const char* rid   = doc["rid"];
    char* shortloc = strtok(rid, "-");

    if (strcmp(event, "leave") == 0) {   // User is leaving a known area, so they must be traveling
      Serial.print(F("User "));
      Serial.print(personid);
      Serial.print(F(" is leaving: "));
      Serial.println(shortloc);
      // strcpy(peopleLocations[personArrIndex], "traveling");
      peopleLocations[personArrIndex] = "traveling";
    } 
    else {  // must be arriving somewhere
      Serial.print(F("User "));
      Serial.print(personid);
      Serial.print(F(" is arriving at: "));
      Serial.println(shortloc);
      // strcpy(peopleLocations[personArrIndex], shortloc);
      peopleLocations[personArrIndex] = shortloc;
    }
  }
  else {
    // some other message type (see doc)
  }
  PrintLocations();
}



// *----
// Function that handles the demo mode pin being set.  It triggers a random clock
// movement every 5 minutes to show off the capabilities of the clock.
// *----
void demoMode () {
  // Move Hands to random location
  Serial.println("Demo Mode.  Moving hands to random location.");
  for (byte i = 0; i < NUM_PEOPLE; i++) {
    peopleLocations[i] = Locations[random(12)];
  }
}



// *----
// Function that takes the desired locations of the hands and translates to a numerical location
// for the stepper motors.  It also adds in some random movements (extra turns each way) to the
// movement.
// *----
void moveMotors() {
  long motorDestinations[NUM_PEOPLE] = {};

  for (byte i=0; i<NUM_PEOPLE; i++) {
    Serial.print("Moving ");
    Serial.print(peopleNames[i]);
    Serial.print(" to position ");
    Serial.println(peopleLocations[i]);

    // Find index of the desired position
    int locationIndex = arrayFind(peopleLocations[i], Locations, 12);
    if (locationIndex < 0) { // not found
      locationIndex = 0;
      Serial.println("No location match found.  Skipping...");
      // die("Error in location match");
      continue;
      // return;
    }

    long desiredPosition = locationIndex * 128;
    long actualPosition = stepperPtrArray[i]->targetPosition();
    long currentPosition = removeRandomness(REVERSE * actualPosition);
    Serial.print(F("Current position: "));
    Serial.println(currentPosition);

    if (currentPosition != desiredPosition) {
      Serial.print("Motor ");
      Serial.print(i);
      Serial.print(": Moving to location: ");
      Serial.println(desiredPosition);

      motorDestinations[i] = REVERSE * addRandomness(desiredPosition);
    }
    else {
      Serial.print("Motor ");
      Serial.print(i);
      Serial.print(": No change from: ");
      Serial.println(desiredPosition);

      motorDestinations[i] = actualPosition;
    }
  }

  // Command the steppers to move 
  moveSteppers(motorDestinations, NUM_PEOPLE);
}



// *----
// A function to add randomness to the hand movement.  Depending on a "random" number,
// a low percentage of the time an extra 2 turns are added, a higher percentage a single
// turn is added, and most of the time nothing is added.
// *----
long addRandomness(long dest) {
  long num = random(999);
  
  // The idea is that most of the time we don't want to do anything to the number sent to the motor
  // however, we want to add a chance that it will turn an extra time or 2 around the clock or change directions.

  if (num < 50) {  // bottom 5% turn 2x to the left
    return dest - (2 * FULL_TURN);
  }
  else if (num >=50 and num < 150) {   // next 10% get 1x to the left
    return dest - FULL_TURN;
  } 
  else if (num >= 150 and num < 850) {   // middle XX do nothing
    return dest;
  }
  else if (num >= 850 and num < 950) {   // 10% 1 extra right turn
    return dest + FULL_TURN;
  }
  else {    // last 5% get 2x turns to the right
    return dest + (2 * FULL_TURN);
  }
}



// *----
// Because we don't want the clock to move every time a random movement is added, this function
// is used to "unrandomize" the position, so that if a random move was made to get to a location
// that it's still understood as that location and not another.
// *----
long removeRandomness(long dest) {
  // so we're not moving all the time, we need to find the modulo of the number of turns we've done.

  // First, undo any reversing that may have been done
  dest = dest;
  if (dest < 1536) {    // work with positive numbers
    dest = dest + (3 * FULL_TURN);
  }
  return dest % FULL_TURN;
}



// a function used to set the HOME location of stepper motors when the Arduino boots.
// this uses terms like "left" and "right" as the motor triggers the limit switches.
// The direction is arbitrary, but essentially detects the rising edge of the limit switch trigger (left),
// moves the motor past the switch, reverses it back to hit the switch again (right) and then takes the 
// average of the 2 step points to create "HOME."


// *----
// This function initializes the stepper pins, initial positions, and parameters as well
// as the microswitches used for positioning.
// *----
bool initializeSteppers() {
  Serial.println(F("Initializing Stepper Motors:"));
  for (byte i=0; i<NUM_PEOPLE; i++) {
    // Initialize all steppers
    // NOTE: The sequence 1-3-2-4 is required for proper sequencing of 28BYJ-48
    stepperPtrArray[i] = new AccelStepper(FULLSTEP, stepperPins[i][0], stepperPins[i][2], stepperPins[i][1], stepperPins[i][3]);
    stepperPtrArray[i]->setMaxSpeed(MaxSpeed);
    stepperPtrArray[i]->setAcceleration(Accel);
    stepperPtrArray[i]->setCurrentPosition(0);
    stepperPtrArray[i]->moveTo(MAX_POSITION);
  
    // Initialize all switches
    switchPtrArray[i] = new ezButton(switchPins[i]);
    switchPtrArray[i]->setDebounceTime(50); // set debounce time to 50 milliseconds
  }
}



// *----
// This is the primary function used to move the steppers.  It's a bit complicated because it allows for
// the simultaneous movements of multiple motors.  The idea is that of the array of motors, a random index list
// is generated and placed into the "running array" of simulataneous motor moves.  As each motor completes its move,
// another motor is added to the "running array" until all motors have moved to the desired location.
//
// There is a special flag for motorInitialize that can be set true to utilize the microswitches and set the motor's
// "home" position.  This is needed because steppers don't remember their position when power is removed.
// *----
bool moveSteppers(long * destinations, int size, bool motorInitialize = false) {  

  bool F_allMotorsMoved = false;
  bool F_motorNeedsMoving = true;
  bool F_motorError = false;
  bool motorsAtLocation[NUM_PEOPLE] = {false, false, false, false, false};

  if (motorInitialize == false) {   // if we're not in the startup initialization
    // set each motor's new destination
    for (byte i=0; i<size; i++) {
      stepperPtrArray[i]->moveTo(destinations[i]);
      if (stepperPtrArray[i]->distanceToGo() == 0) {
        motorsAtLocation[i] = true;
      }
    }
  }

  // Initialize the running motor array list
  int motorRunArr[MAX_MOTOR_RUN] = {};
  for (int i=0; i<MAX_MOTOR_RUN; i++) {
    motorRunArr[i] = -1;
  }
  // list of motor IDs that are then shuffled
  int motorIndexArray[NUM_PEOPLE] = {0,1,2,3,4};
  shuffleArray(motorIndexArray, NUM_PEOPLE);

  #ifdef DEBUG    // DEBUGGING
  Serial.print(F("motorIndexArray:  "));
  for (byte i=0; i<NUM_PEOPLE; i++) {
    Serial.print(motorIndexArray[i]);
    Serial.print(" ");
  }
  Serial.println();
  
  Serial.println("Initialized arrays: ");
  Serial.print(F("motorRunArr: "));
  printArray(motorRunArr, MAX_MOTOR_RUN);
  Serial.print(F("motorsAtLocation: "));
  printArray(motorsAtLocation, NUM_PEOPLE);
  Serial.print(F("motor Destinations: "));
  printArray(destinations, NUM_PEOPLE);
  #endif


  while (F_allMotorsMoved == false) {
    // populate the running motor array only if the flag is set
    if (F_motorNeedsMoving == true) {
      for (int i=0; i<MAX_MOTOR_RUN; i++) {
        if (motorRunArr[i] == -1) {      // found empty slot

          // loop through the randomized list of motorIDs
          for (int j=0; j<NUM_PEOPLE; j++) {
            #ifdef DEBUG    // DEBUGGING
            Serial.print("Iterators: motorRunArr: ");
            Serial.print(i);
            Serial.print(" -- motorIndexArray: ");
            Serial.println(j);
            #endif

            // Check to see if we already have this motor in the init loop
            if (arrayFind(motorIndexArray[j], motorRunArr, MAX_MOTOR_RUN) > -1) {
              //Serial.println("Found a duplicate"); 
              continue;            
            }
            
            // If the motor number is not initialized (0) and not already in the init array, add it.
            if (motorsAtLocation[motorIndexArray[j]] == false) {
              motorRunArr[i] = motorIndexArray[j];
              //Serial.println("Added value to motorRunArr");
              break;
            }
          }
        }      
      }
      F_motorNeedsMoving = false;
      #ifdef DEBUG    // DEBUGGING
      Serial.println("After population:");
      Serial.print(F("motorRunArr: "));
      printArray(motorRunArr, MAX_MOTOR_RUN);
      Serial.print(F("motorsAtLocation: "));
      printArray(motorsAtLocation, NUM_PEOPLE);
      #endif
    }

    // Initialize the switch loops for active motors
    for (int i=0; i<MAX_MOTOR_RUN; i++) {
      if (motorRunArr[i] == -1) {
        continue;
      }

      if (motorInitialize) {
        switchPtrArray[motorRunArr[i]]->loop(); // MUST call the loop() function first

        // Catch switch press actions
        if (switchPtrArray[motorRunArr[i]]->isPressed()) {
          switchPress(motorRunArr[i]);
        }
      }

      // catch motor stopped conditions
      // If the stepper has arrived at a position (distanceToGo == 0):
      if (stepperPtrArray[motorRunArr[i]]->distanceToGo() == 0) {     // motor has reached the destination.  Which one?
        int motorStatus = motorStopped(motorRunArr[i], motorInitialize);
        
        // Check status returned
        if (motorStatus > 0) {        // motor is initialized
          #ifdef DEBUG    // DEBUGGING
            Serial.print(F("Motor "));
            Serial.print(motorRunArr[i]);
            Serial.println(" is at desired location.  Setting motorsAtLocation to true and init slot to -1");
          #endif

          motorsAtLocation[motorRunArr[i]] = true;    // mark the motor as initialized
          motorRunArr[i] = -1;     // reset array value to init another motor
          F_motorNeedsMoving = true;

          #ifdef DEBUG    // DEBUGGING
            Serial.print(F("motorRunArr: "));
            printArray(motorRunArr, MAX_MOTOR_RUN);
            Serial.print(F("motorsAtLocation: "));
            printArray(motorsAtLocation, NUM_PEOPLE);
          #endif
        }
        else if (motorStatus < 0) {   // motor init failure
          motorsAtLocation[motorRunArr[i]] = false;    // mark the motor as failed
          F_motorNeedsMoving = false;
          F_motorError = true;
        }
      }
      else {
        stepperPtrArray[motorRunArr[i]]->run(); // MUST be called in loop() function
      }
    }

    // Check for error
    if (F_motorError) {
      return false;
    }

    // check for completion
    int checkCount = 0;
    for (byte i=0; i<NUM_PEOPLE; i++) {
      if (motorsAtLocation[i] == true) {
        checkCount++;
      }
    }
    if (checkCount == NUM_PEOPLE) {
      F_allMotorsMoved = true;
    }
  }

  return true;
}



// Shuffle an array's values
void shuffleArray(int * array, int size) {
  int last = 0;
  int temp = array[last];
  for (int i=0; i<size; i++)
  {
    int index = random(size);
    array[last] = array[index];
    last = index;
  }
  array[last] = temp;
}



// Check for a value in an array (function overloaded to handle multiple types of arrays)
int arrayFind(int needle, int * haystack, int size) {
  for (byte i=0; i<size; i++) {
    if (needle == haystack[i]) {
      return i;
    }
  }
  return -1;
}
int arrayFind(const char *& needle, const char * haystack[12], int size) {
  for (byte i=0; i<size; i++) {
    if (strcmp(needle, haystack[i]) == 0) {
      return i;
    }
  }
  return -1;
}
int arrayFind(String needle, String haystack[12], int size) {
  for (byte i=0; i<size; i++) {
    if (needle == haystack[i]) {
      return i;
    }
  }
  return -1;
}



// function to print an array (function is overloaded to handle multiple array types)
void printArray( long * arr, int size) {
  for (int i=0; i<size; i++) {
    Serial.print(arr[i]);
    Serial.print(" ");
  }
  Serial.println("");
}
void printArray( int * arr, int size) {
  for (int i=0; i<size; i++) {
    Serial.print(arr[i]);
    Serial.print(" ");
  }
  Serial.println("");
}
void printArray( bool * arr, int size) {
  for (int i=0; i<size; i++) {
    Serial.print(arr[i]);
    Serial.print(" ");
  }
  Serial.println("");
}



// *----
// a function used to set the movement of stepper motors when the limit switch is pressed.
// this uses terms like "left" and "right" as the motor triggers the limit switch.
// The direction is arbitrary, but essentially detects the rising edge of the limit switch trigger (left),
// moves the motor past the switch, reverses it back to hit the switch again (right) and then takes the 
// average of the 2 step points to create "HOME."
// *----
void switchPress (byte switchIndex) {
  // A switch has been pressed on the particular index
  #ifdef DEBUG    // DEBUGGING
    Serial.print(F("Limit switch: "));
    Serial.print(switchIndex);
    Serial.println(F(" is TOUCHED"));
  #endif

  if (stepperTouchCount[switchIndex] == 0) {    // 1st time we touch a switch
    stepperTouchCount[switchIndex]++;
    // Save current position
    stepperLeftPos[switchIndex] = stepperPtrArray[switchIndex]->currentPosition();
    
    #ifdef DEBUG    // DEBUGGING
      Serial.print(F("Stepper "));
      Serial.print(switchIndex);
      Serial.print(F(" 1st touch position: "));
      Serial.println(stepperLeftPos[switchIndex]);
    #endif

    // Set the new move past the limit switch to come back
    stepperPtrArray[switchIndex]->moveTo(stepperLeftPos[switchIndex] + 200);
  }
  else if (stepperTouchCount[switchIndex] == 1) {   // 2nd time we touched
    stepperTouchCount[switchIndex]++;
    // Save current position
    stepperRightPos[switchIndex] = stepperPtrArray[switchIndex]->currentPosition();

    #ifdef DEBUG    // DEBUGGING
      Serial.print(F("Stepper "));
      Serial.print(switchIndex);
      Serial.print(F(" 2nd touch position: "));
      Serial.println(stepperRightPos[switchIndex]);
    #endif

    // Set the new move to the center between the limit touch points
    stepperPtrArray[switchIndex]->moveTo((stepperLeftPos[switchIndex] + stepperRightPos[switchIndex])/2);
  }
  // Not sure if we will hit these conditions
  else if (stepperTouchCount[switchIndex] > 1) {    // additional switch touches
    #ifdef DEBUG    // DEBUGGING
      Serial.print(F("Stepper "));
      Serial.print(switchIndex);
      Serial.println(F(" touched more than 2x - are we home?"));
    #endif
  }
}



// *----
// A function that is used during initialization to tell if a motor has reached its "home" position.
// In normal running, once the motor has reached its destination, the outputs are disabled to save 
// power.
// *----
int motorStopped(int motorIndex, bool motorInitialize = false) {
  if (motorInitialize) {      // motors need initialization, so search for switch presses
    // skip anything > 2
    if (stepperTouchCount[motorIndex] == 2) { // if motor moved to the HOME position
      stepperPtrArray[motorIndex]->setCurrentPosition(0);   // reset position to 0
      stepperPtrArray[motorIndex]->disableOutputs();
          
      // HOMED stepper :-)
      Serial.print(F("Stepper "));
      Serial.print(motorIndex);
      Serial.println(F(" is HOME"));

      return 1;
    }
    else if (stepperTouchCount[motorIndex] == 1) {     // motor touched once, moved past and needs to return
      stepperPtrArray[motorIndex]->moveTo(-MAX_POSITION);   // return back to touch the right side of the switch

      #ifdef DEBUG    // DEBUGGING    
        Serial.print(F("Stepper "));
        Serial.print(motorIndex);
        Serial.println(F(" past switch, reversing"));
      #endif

      return 0;
    }
    else if (stepperTouchCount[motorIndex] == 0) {    // Reached the MAXTRAVEL without hitting a switch.  Likely a problem has happened
      stepperPtrArray[motorIndex]->disableOutputs();
      return -1;
    }
  }
  else {      // motors are already initialized, so assume that when a motor has reached it's location that it's where it should be
    stepperPtrArray[motorIndex]->disableOutputs();

    // HOMED stepper :-)
    Serial.print(F("Stepper "));
    Serial.print(motorIndex);
    Serial.println(F(" is AT DESIRED LOCATION"));

    return 1;
  }
}



#ifdef ENABLE_WIFI
// *----
// Connect/Reconnect to the MQTT Server
// *----
void reconnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a descriptive client ID
    String clientId = "ArduinoClient";
    // Attempt to connect
    if (mqttClient.connect(clientId.c_str(), mqtt_username, mqtt_password)) {
      Serial.println("connected");
      mqttClient.subscribe("owntracks/whereabouts/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}



// *----
// Function to print the Wifi status to the serial port
// *----
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.println(F("WiFi Data:"));
  Serial.print(F("\tSSID: "));
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  Serial.print(F("\tIP Address: "));
  Serial.println(WiFi.localIP());

  // print the received signal strength:
  Serial.print(F("\tSignal strength (RSSI): "));
  Serial.print(WiFi.RSSI());
  Serial.println(F(" dBm\n"));
}
#endif



// *----
// A function to print out the time from the Real-Time Clock (RTC) module
// *----
void printRtcTime() {
  DateTime dt = rtc.now();

  // For leading zero look to DS3231_dateformat example
  Serial.print(F("RTC data: "));
  Serial.print(dt.year());   Serial.print("-");
  Serial.print(dt.month());  Serial.print("-");
  Serial.print(dt.day());    Serial.print(" ");
  Serial.print(dt.hour());   Serial.print(":");
  Serial.print(dt.minute()); Serial.print(":");
  Serial.print(dt.second()); Serial.println("");
  Serial.print(F("RTC timestamp: "));
  Serial.println(dt.unixtime());
  Serial.println("");
}



// *----
// A function to update the RTC module time from the NTP server.
// *----
void updateRtcFromNtp() {
  timeClient.update();  // Retrieve current epoch time from NTP server

  if (timeClient.updated()) {
  // Serial.println("UTC : " + timeClient.getFormattedUTCDateTime());
  // Serial.println("UTC EPOCH : " + String(timeClient.getUTCEpochTime()));

  // unsigned long epoch = timeClient.getUTCEpochTime();
  // Serial.println("dt epoch : " + String(epoch));
  // DateTime dt = DateTime(epoch);
  // Serial.println("dt time : " + dt.timestamp());

    rtc.adjust(DateTime(timeClient.getEpochTime()));  // Set RTC time using NTP epoch time
    // note the time we last synced with NTP
    lastClockSync = rtc.now().unixtime();
    Serial.println(F("Time from RTC after update: "));
    printRtcTime();
  }
  else {
    Serial.println(F("ERROR: timeClient did not get the time from NTP."));
    die("Unable to get NTP time.  Retry...");
  }
}



// *----
// Print out the location array to the serial port
// *----
void PrintLocations () {
  unsigned long time = rtc.now().unixtime();
  Serial.println(F("User Data:"));
  for (int i = 0; i < NUM_PEOPLE; i++) {
    Serial.print("\t");
    Serial.print(peopleNames[i]);
    Serial.print(": ");
    Serial.print(peopleLocations[i]);
    Serial.print("\t(");
    Serial.print((time - peopleTimestamps[i])/60, DEC);
    Serial.println(" mins ago)"); 
  }
}



// *----
// A function called during setup to initialize the LED pins and make them blink once
// *----
void setupLed () {
  // Go through the LEDs and turn them all on
  pinMode(WIFI_LED, OUTPUT);
  digitalWrite(WIFI_LED, HIGH);
  pinMode(DATA_LED, OUTPUT);
  digitalWrite(DATA_LED, HIGH);
  pinMode(ERR_LED, OUTPUT);
  digitalWrite(ERR_LED, HIGH);
  pinMode(DEMO_PIN, INPUT);

  // delay a little bit
  delay(750);

  // Turn them all off.
  digitalWrite(WIFI_LED, LOW);
  digitalWrite(DATA_LED, LOW);
  digitalWrite(ERR_LED, LOW);

  return;
}



// *----
// Blink an LED on and off for the specified time interval up to the totalTime that we want it blinked.
// for example, a time of 500 and totalTime of 5 would blink the LED on and off every .5 seconds for 5 seconds
// I used this to give some meaning to delay events.
// *----
void blinkLed (byte ledPin, unsigned long time, long totalTime) {
  // Convert the totalTime to milliseconds
  long count = long(totalTime * 1000);

  // Blink until the timer runs out
  while (count > 0) {
    digitalWrite(ledPin, !digitalRead(ledPin));
    delay(time);
    count = count - time;
  }
  return;
}



// *----
// Function to blink an error LED for a while and then reset to try again
// *----
bool die(const char* message, bool retry=true) {
  Serial.println(message);

  #ifdef ENABLE_WIFI
    mqttClient.disconnect();
  #endif

  if (retry) {
    // Blink the red LED in .5 sec intervals for 1 min and reset and try again.
    blinkLed(ERR_LED, 500, 60);
    resetFunc();
  }
  else {
    digitalWrite(ERR_LED, HIGH);
    while(true);
  }
}